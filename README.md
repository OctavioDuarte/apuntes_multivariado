# Apuntes de Análisis Multivariado

Estos apuntes fueron creados por el dueño del repositorio (Octavio Martín Duarte) durante la cursada de la Especialización en Estadística Matemática de la UBA. 
Las clases pertenecen a la docente Graciela Boente y a su ayudante Jemina García. 

En general estos apuntes contienen lo que está en los PDF, más las aclaraciones dadas por la profesora en clase, más material que extraje de los libros. 

Típicamente son mucho más largos que lo visto en clase, siguiendo un estilo de "ninguna aclaración está de más y ningún paso es obvio". 

Cualquier consejo sobre mejoras, detalles, errores, etc es bienvenido y puede enviarse a mi correo personal o, mucho mejor, como un commit. 

# Estado del Proyecto (Calidad de los apuntes)

Estos apuntes fueron hechos con mucho amor y esfuerzo y me permitieron aprobar muy cómodamente la cursada. Volví a revisar y leer algunas cosas un par de veces, pero de ninguna manera son finales ni deben ser leídos a ciegas. 
Cualquier revisión o pregunta es muy bienvenida, aunque puede que no tenga tiempo de incorporarla inmediatamente porque necesitaría repasar algunas cosas para entender y ponerme de acuerdo con la sugerencia (a menos que venga de Graciela o autoridad equivalente, en cuyo caso haré el cambio y después me convenceré de por qué es adecuado).

## Estructura de los archivos

* Los apuntes en PDf está en el directorio principal.
* El código fuente está en la carpeta `./códigoFuente` en dos formatos, `.lyx` y `.tex`.

Los apuntes fuero escritos en LyX, que es básicamente LaTeX. 
Los archivos de referencia por ahora son los `.lyx`, pero dejo los `.tex` por si alguien no sabe usar LyX pero quiere realizar cambios en el código fuente. En ese caso, por favor comuníquense conmigo y yo agrego los cambios al archivo de referencia. 

# Preguntas Frecuentes

## ¿Dónde está la clase 8?

Fue una clase práctica, por lo que no hice apunte. 
Hay continuidad temática entre la 7 y la 9.
