#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass amsart
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language spanish-mexico
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style french
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Hotelling No central
\end_layout

\begin_layout Lemma
Para un modelo lineal
\end_layout

\begin_layout Lemma
\begin_inset Formula 
\[
\boldsymbol{y}=K\boldsymbol{\beta}+\boldsymbol{\varepsilon}
\]

\end_inset


\end_layout

\begin_layout Lemma
\begin_inset Formula $K\in\mathbb{R}^{p\times p}$
\end_inset


\end_layout

\begin_layout Lemma
\begin_inset Formula $\text{rg}\left(K\right)=p$
\end_inset


\end_layout

\begin_layout Lemma
\begin_inset Formula $\boldsymbol{e}\sim\mathcal{N}\left(\boldsymbol{0};\sigma^{2}I_{n}\right)$
\end_inset


\end_layout

\begin_layout Lemma
\begin_inset Formula $\boldsymbol{\rho}=\lVert\boldsymbol{y}-K\hat{\boldsymbol{\beta}}\rVert^{2}=\min_{\boldsymbol{b}}\lVert\boldsymbol{y}-K\boldsymbol{b}\rVert^{2}$
\end_inset


\end_layout

\begin_layout Lemma
Entonces
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\boldsymbol{\rho}=\nicefrac{1}{W^{11}}$
\end_inset

 donde 
\begin_inset Formula $W=\left(\begin{matrix}\boldsymbol{y}'\\
K'
\end{matrix}\right)\cdot\left(\begin{matrix}\boldsymbol{y} & K\end{matrix}\right)=\left(\begin{matrix}\boldsymbol{y}'\boldsymbol{y} & \boldsymbol{y}'K\\
K'\boldsymbol{y} & K'K
\end{matrix}\right)$
\end_inset


\end_layout

\begin_layout Enumerate

\end_layout

\begin_layout Proof
.
\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\hat{\beta}=\left(K'K\right)^{-1}K'\boldsymbol{y}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\lVert\boldsymbol{y}-K\hat{\beta}\rVert^{2}=\lVert\boldsymbol{y}\rVert^{2}-\lVert K\hat{\beta}\rVert^{2}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\lVert\boldsymbol{y}-K\hat{\beta}\rVert^{2}=\boldsymbol{y}'\boldsymbol{y}-\left(K\hat{\beta}\right)'\left(K\hat{\beta}\right)
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\lVert\boldsymbol{y}-K\hat{\beta}\rVert^{2}=\boldsymbol{y}'\boldsymbol{y}-\left(\left(K'K\right)^{-1}K'\boldsymbol{y}\right)'K'K\left(\left(K'K\right)^{-1}K'\boldsymbol{y}\right)
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\boldsymbol{a}=\boldsymbol{y}'\boldsymbol{y}-\boldsymbol{y}'K\left(K'K\right)^{-1}K'\boldsymbol{y}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\boldsymbol{a}=\boldsymbol{y}'\left(I-K\left(K'K\right)^{-1}K'\right)\boldsymbol{y}
\]

\end_inset


\end_layout

\begin_layout Proof
ES de proyección 
\begin_inset Formula $\left(I-K\left(K'K\right)^{-1}K'\right)=A_{1}$
\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
A_{1}\boldsymbol{y}=A_{1}K\beta+A_{1}\boldsymbol{e}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
A_{1}\boldsymbol{y}=0+A_{1}\boldsymbol{e}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\boldsymbol{e}\sim\mathcal{N}\left(0;\sigma^{2}\right)
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
a=\boldsymbol{y}'\boldsymbol{y}-\boldsymbol{y}'K\left(K'K\right)^{-1}K'\boldsymbol{y}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
a=W_{11}-W_{22}W_{22}^{-1}W_{21}
\]

\end_inset


\end_layout

\begin_layout Proof
En la condicional, 
\begin_inset Formula $W_{11.2}=W_{11}-W_{22}W_{22}^{-1}W_{21}$
\end_inset

, ya sabemos que 
\begin_inset Formula $W^{11}=\frac{1}{W_{11.2}}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Section
Inferencia
\end_layout

\begin_layout Standard
Hay que probar que 
\begin_inset Formula $\boldsymbol{y}=\sqrt{n}\left(\overline{X}-\boldsymbol{\mu}\right)$
\end_inset

 es normal y que 
\begin_inset Formula $\left(n-1\right)S$
\end_inset

 es wishart.
\end_layout

\begin_layout Subsection
Veamos que la familia de las 
\begin_inset Formula $\mathcal{N}_{n}\left(\boldsymbol{\mu};\Sigma\right)$
\end_inset

 es exponencial.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\prod_{I}f_{x_{i}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\prod_{I}\frac{1}{\left(2\pi\right)^{\frac{n}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{1}{2}}}e^{-\frac{1}{2}\left(x_{i}-\mu\right)'\Sigma^{-1}\left(x_{i}-\mu\right)}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{n}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{-\sum_{I}\frac{1}{2}\left(x_{i}-\mu\right)'\Sigma^{-1}\left(x_{i}-\mu\right)}
\]

\end_inset


\end_layout

\begin_layout Standard
el exponente es escalar, igual a su traza
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{n}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{\text{tr}\left[-\sum_{I}\frac{1}{2}\left(x_{i}-\mu\right)'\Sigma^{-1}\left(x_{i}-\mu\right)\right]}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{n}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{-\frac{1}{2}\sum_{I}\text{tr}\left[\Sigma^{-1}\left(x_{i}-\mu\right)\left(x_{i}-\mu\right)'\right]}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{n}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{\text{tr}\left[\Sigma^{-1}\sum_{i=1}^{n}\left(x_{i}-\mu\right)\left(x_{i}-\mu\right)'\right]}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\sum_{i=1}^{n}\left(x_{i}-\mu\right)\left(x_{i}-\mu\right)'=\sum\left[\left(x_{i}-\overline{x}\right)+\left(\overline{x}-\mu_{i}\right)\right]\left[\left(x_{i}-\overline{x}\right)+\left(\overline{x}-\mu_{i}\right)\right]$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\sum_{i=1}^{n}\left(x_{i}-\mu\right)\left(x_{i}-\mu\right)'=\underbrace{\sum\left(x_{i}-\overline{x}\right)\left(x_{i}-\overline{x}\right)'}_{Q}+\underbrace{\left(\overline{x}-\mu_{i}\right)\sum\left(x_{i}-\overline{x}\right)'+\left[\sum\left(x_{i}-\overline{x}\right)\right]\left(\overline{x}-\mu\right)'}_{0}+\left(\overline{x}-\mu\right)\left(\overline{x}-\mu\right)'n$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\sum_{i=1}^{n}\left(x_{i}-\mu\right)\left(x_{i}-\mu\right)'=Q+\left(\overline{x}-\mu\right)\left(\overline{x}-\mu\right)'n$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{\text{tr}\left[\Sigma^{-1}Q+\Sigma^{-1}\left(\overline{x}-\mu\right)\left(\overline{x}-\mu\right)'n\right]}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{\text{tr}\left[\Sigma^{-1}Q\right]+n\text{tr}\left[\Sigma^{-1}\left(\overline{x}-\mu\right)\left(\overline{x}-\mu\right)'\right]}
\]

\end_inset


\end_layout

\begin_layout Standard
el último término es escalar
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{\text{tr}\left[\Sigma^{-1}Q\right]+n\Sigma^{-1}\left(\overline{x}-\mu\right)\left(\overline{x}-\mu\right)'}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x};\boldsymbol{\mu};\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{-\frac{1}{2}\left\{ \text{tr}\left[\Sigma^{-1}Q\right]+\frac{1}{2}n\Sigma^{-1}\left(\overline{x}-\mu\right)\left(\overline{x}-\mu\right)\right\} '}
\]

\end_inset


\end_layout

\begin_layout Standard
esta es una familia exponencial con estadísticos suficientes 
\begin_inset Formula $Q$
\end_inset

 y 
\begin_inset Formula $\overline{x}$
\end_inset

.
\end_layout

\begin_layout Standard
Los parámetros de la familia exponencial son 
\begin_inset Formula $\Sigma^{-1}$
\end_inset

 y 
\begin_inset Formula $\mu$
\end_inset

.
\end_layout

\begin_layout Standard
El espacio natural es 
\begin_inset Formula $\Lambda=\left\{ \boldsymbol{\mu};\left(\sigma_{ij}\right)_{i\leq j}\right\} /\sigma_{ii}>0$
\end_inset

.
\end_layout

\begin_layout Standard
Este conjunto contiene una bola.
\end_layout

\begin_layout Subsection
Estimadores de máxima verosimilitud.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
L\left(\mu;\Sigma\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\Sigma\right|^{\frac{n}{2}}}e^{-\frac{1}{2}\left\{ \text{tr}\left[\Sigma^{-1}Q\right]+\frac{1}{2}n\Sigma^{-1}\left(\overline{x}-\mu\right)\left(\overline{x}-\mu\right)\right\} '}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{\Sigma}=\frac{Q}{n}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
L\left(\hat{\mu};\hat{\Sigma}\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\hat{\Sigma}\right|^{\frac{n}{2}}}e^{-\frac{1}{2}\left\{ \text{tr}\left[\Sigma^{-1}Q\right]+\frac{1}{2}n\Sigma^{-1}\left(\overline{x}-\hat{\mu}\right)\left(\overline{x}-\hat{\mu}\right)\right\} '}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\overline{x}-\hat{\mu}\right)=0$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
L\left(\hat{\mu};\hat{\Sigma}\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\hat{\Sigma}\right|^{\frac{n}{2}}}e^{\text{tr}\left[\Sigma^{-1}Q\right]}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{tr}\left[\Sigma^{-1}Q\right]=\text{tr}\left[Q^{-1}nQ\right]=np$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
L\left(\hat{\mu};\hat{\Sigma}\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\hat{\Sigma}\right|^{\frac{n}{2}}}e^{-\frac{np}{2}}
\]

\end_inset


\end_layout

\begin_layout Standard
Veamos que estos desarrollos efectivamente son los estimadores de máxima
 verosimiulitud
\end_layout

\begin_layout Standard
trabajamos con la logverosimilituyd
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G\left(\mu;\Sigma\right)=\ln\left[L\left(\hat{\mu};\hat{\Sigma}\right)+\frac{np}{2}\ln2\pi\right]
\]

\end_inset


\end_layout

\begin_layout Standard
con el segundotérmino para ahorar constantes sin perder generalidad
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G\left(\mu;\Sigma\right)=-\frac{n}{2}\ln\left|\Sigma\right|-\frac{1}{2}\text{tr}\left[\Sigma^{-1}Q\right]-\frac{n}{2}\left(\overline{x}-\hat{\mu}\right)\Sigma^{-1}\left(\overline{x}-\hat{\mu}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{máx}_{\left(\mu;\Sigma\right)}G\left(\mu;\Sigma\right)=-\frac{n}{2}\ln\left|\Sigma\right|-\frac{1}{2}\text{tr}\left[\Sigma^{-1}Q\right]-\frac{n}{2}\left(\overline{x}-\hat{\mu}\right)\Sigma^{-1}\left(\overline{x}-\hat{\mu}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{máx}_{\left(\mu;\Sigma\right)}G\left(\mu;\Sigma\right)=-\frac{n}{2}\ln\left|\Sigma\right|-\frac{1}{2}\text{tr}\left[\Sigma^{-1}Q\right]-\frac{n}{2}\left(\overline{x}-\hat{\mu}\right)\Sigma^{-1}\left(\overline{x}-\hat{\mu}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{máx}_{\left(\mu;\Sigma\right)}G\left(\mu;\Sigma\right)=\text{mín}{}_{\left(\mu;\Sigma\right)}-\frac{2G\left(\mu;\Sigma\right)}{m}=\text{mín}{}_{\left(\mu;\Sigma\right)}G_{1}\left(\mu;\Sigma\right)$
\end_inset


\end_layout

\begin_layout Standard
evaluemos g1 en los estimados, primero la media
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\Sigma\right)=\ln\left|\Sigma\right|+\text{tr}\left[\Sigma^{-1}\hat{\Sigma}\right]+\left(\overline{x}-\mu\right)'\Sigma^{-1}\left(\overline{x}-\mu\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\Sigma\right)=\ln\left|\Sigma\right|+\text{tr}\left[\Sigma^{-1}\hat{\Sigma}\right]+\underbrace{\left(\overline{x}-\mu\right)'\Sigma^{-1}\left(\overline{x}-\mu\right)}_{\geq0}
\]

\end_inset


\end_layout

\begin_layout Standard
Y sñoo es cero en el estimador
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\Sigma\right)\geq G_{1}\left(\hat{\mu};\hat{\Sigma}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Buscamos la matriz 
\begin_inset Formula $\sigma$
\end_inset

 que minimiza esto
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\hat{\Sigma}\right)=\ln\left|\Sigma\right|+\text{tr}\left[\Sigma^{-1}\hat{\Sigma}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
Queremos lograr que para todo vaor de 
\begin_inset Formula $\Sigma$
\end_inset

 distinto de 
\begin_inset Formula $\hat{\Sigma}$
\end_inset

 el 
\begin_inset Formula $G_{1}$
\end_inset

 sea mayor que el observado.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\hat{\Sigma}\right)=\ln\left|\Sigma\right|-\ln\left|\hat{\Sigma}\right|+\text{tr}\left[\hat{\Sigma}^{-1}\hat{\Sigma}\right]-p
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Sigma=CC'$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Sigma^{-1}=\left(C'\right)^{-1}C^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{tr}\left(\Sigma^{-1}\hat{\Sigma}\right)=\text{tr}\left[\left(C'\right)^{-1}C^{-1}\hat{\Sigma}\right]$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{tr}\left(\Sigma^{-1}\hat{\Sigma}\right)=\text{tr}\left[C^{-1}\hat{\Sigma}\left(C'\right)^{-1}\right]$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $U=C^{-1}\hat{\Sigma}\left(C'\right)^{-1}$
\end_inset

 como la 
\begin_inset Formula $\hat{\Sigma}$
\end_inset

 es wishart, es definida positiva con probabilidad 1.
 Además, U es simétrica.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{tr}\left(\hat{\Sigma}^{-1}\hat{\Sigma}\right)=\sum\theta_{i}
\]

\end_inset


\end_layout

\begin_layout Standard
Tomamos por traza la suma de los autovaores.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\hat{\Sigma}\right)=-\ln\frac{\left|\hat{\Sigma}\right|}{\left|\Sigma\right|}+\underbrace{\text{tr}\left[\hat{\Sigma}^{-1}\hat{\Sigma}\right]}_{\sum\theta_{i}-1}-p
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\hat{\Sigma}\right)=-\ln\left|\Sigma^{-1}\hat{\Sigma}\right|+\underbrace{\text{tr}\left[\hat{\Sigma}^{-1}\hat{\Sigma}\right]}_{\sum\theta_{i}-1}-p
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\hat{\Sigma}\right)=-\ln\left|U\right|+\underbrace{\text{tr}\left[\hat{\Sigma}^{-1}\hat{\Sigma}\right]}_{\sum\theta_{i}-1}-p
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left|U\right|=\prod\theta_{i}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\hat{\Sigma}\right)=-\ln\prod\theta_{i}+\underbrace{\text{tr}\left[\hat{\Sigma}^{-1}\hat{\Sigma}\right]-p}_{\sum\theta_{i}-1}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
G_{1}\left(\hat{\mu};\hat{\Sigma}\right)=-\sum\ln\theta_{i}+\sum\left(\theta_{i}-1\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $H\left(\Sigma\right)=\sum\left(\theta_{i}-1-\ln\theta_{i}\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $x\geq0\Rightarrow x\leq e^{x-1}\Rightarrow\ln x\leq x-1$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
H\left(\Sigma\right)\geq0
\]

\end_inset


\end_layout

\begin_layout Standard
Entonces tenemos los mínimos de 
\begin_inset Formula $G_{1}$
\end_inset

 y los máximos de 
\begin_inset Formula $G$
\end_inset

.
\end_layout

\end_body
\end_document
