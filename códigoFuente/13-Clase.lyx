#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass amsart
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language spanish-mexico
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style french
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Clase 13: Componentes Principales
\end_layout

\begin_layout Itemize
Esta vez es una sola población.
\end_layout

\begin_layout Itemize
Vamos a medir pérdida de información.
\end_layout

\begin_layout Standard
En principio no lo puedo tratar como un problema de regresion porque no
 hay una subordinación funcional clara.
\end_layout

\begin_layout Standard
La distancia empleada esta vez es la distancia en dirección ortogonal a
 la recta.
 ESto se hace porque no hay variables (dimensiones) obvias.
\end_layout

\begin_layout Section
Definiciones
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{x}\right)=\boldsymbol{\mu}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{x}\right)=\Sigma
\]

\end_inset


\end_layout

\begin_layout Standard
Los autovalores son 
\begin_inset Formula $\left(\lambda_{i}\right)$
\end_inset

 ordenados de mayor a menor.
\end_layout

\begin_layout Standard
Los 
\begin_inset Formula $\boldsymbol{\gamma}_{i}$
\end_inset

 son los autovectores asociados a los respectivos 
\begin_inset Formula $\lambda$
\end_inset

.
 Forman la matriz ortogonal 
\begin_inset Formula $\Gamma$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\Gamma'\Sigma\Gamma=\Lambda
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\Sigma=\sum_{j=1}^{p}\lambda_{j}\boldsymbol{\gamma}_{j}\boldsymbol{\gamma}_{j}'=\Gamma\Lambda\Gamma'
\]

\end_inset


\end_layout

\begin_layout Standard
Podemos escribir al vector 
\begin_inset Formula $\boldsymbol{x}$
\end_inset

 centrado en el nuevo sistema de coordenadas ortogonales.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}-\boldsymbol{\mu}=\sum_{j=1}^{p}\boldsymbol{\gamma}_{j}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\boldsymbol{\gamma}_{j}'
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}=\boldsymbol{\mu}+\sum_{j=1}^{p}\boldsymbol{\gamma}_{j}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\boldsymbol{\gamma}_{j}'
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{v}=
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{v}\right)=\Gamma\mathbb{E}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)=O
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{V}\left(\boldsymbol{v}\right)=\Gamma'\Sigma\Gamma=\Lambda
\]

\end_inset


\end_layout

\begin_layout Standard
Las direcciones principales 
\begin_inset Formula $\boldsymbol{v}_{j}$
\end_inset

 son no correlacionadas de varianza 
\begin_inset Formula $\lambda_{j}$
\end_inset

.
\end_layout

\begin_layout Standard
Por lo tanto, con tal de que 
\begin_inset Formula $\boldsymbol{x}$
\end_inset

 sea normal las 
\begin_inset Formula $\boldsymbol{v}_{j}$
\end_inset

 son independientes.
\end_layout

\begin_layout Subsection
Disgresión Teórica, el sistema en cada una de las observaciones
\end_layout

\begin_layout Standard
Si se diera 
\begin_inset Formula $\left(\boldsymbol{x}_{i}\right)_{i=1}^{n}$
\end_inset

 una m.a.
 iid 
\begin_inset Formula $\sim\boldsymbol{x}$
\end_inset

.
 Si teóricamente conocemos 
\begin_inset Formula $\boldsymbol{\mu}$
\end_inset

 y 
\begin_inset Formula $\Sigma$
\end_inset

, conozco 
\begin_inset Formula $\boldsymbol{\gamma}_{j}$
\end_inset

.
 Ahora 
\begin_inset Formula $\boldsymbol{v}_{i;j}=\boldsymbol{\gamma}'_{j}\left(\boldsymbol{x}_{i}-\boldsymbol{\mu}\right)$
\end_inset

.
\end_layout

\begin_layout Standard
Graficando sobre las dos componentes principales, siempre debería quedar
 una elipse con ejes paralelos a los ejes elegidos para grafivar, los dosprimero
s gamma y además sabemos siempre que el de menor índice concentra una mayor
 variabilidad.
 
\end_layout

\begin_layout Standard
Esto se debe a la no correlación.
\end_layout

\begin_layout Itemize
Es usual estandarizar en este nuevo sistema estandarizar para tener varianza
 1.
 
\begin_inset Formula $\boldsymbol{z}_{j}=\frac{\boldsymbol{v}_{j}}{\sqrt{\lambda_{ij}}}=\boldsymbol{\gamma}'_{j}\frac{\boldsymbol{x}_{i}-\boldsymbol{\mu}}{\sqrt{\lambda_{j}}}$
\end_inset


\end_layout

\begin_layout Standard
Si en vez de las 
\begin_inset Formula $\boldsymbol{v}$
\end_inset

 grafico las 
\begin_inset Formula $\boldsymbol{z}$
\end_inset

 logro un gráfico ciurcular.
\end_layout

\begin_layout Subsection
Correlaciones
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\rho\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)=\frac{\text{Cov}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)}{\sqrt{\text{Var}\left(\boldsymbol{x}_{j}\right)\text{Var}\left(\boldsymbol{v}_{l}\right)}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Sigma=\left(\sigma_{ij}\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\rho\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)=\frac{\text{Cov}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)}{\sqrt{\sigma_{jj}\lambda_{l}}}
\]

\end_inset


\end_layout

\begin_layout Standard
Buscamos directamente 
\begin_inset Formula $\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}\right)=\mathbb{E}\left[\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\boldsymbol{v}'\right]$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}\right)=\mathbb{E}\left[\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\boldsymbol{v}'\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}\right)=\mathbb{E}\left[\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\left(\boldsymbol{\gamma}'_{j}\left(\boldsymbol{x}_{i}-\boldsymbol{\mu}\right)\right)'\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}\right)=\mathbb{E}\left[\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\left(\boldsymbol{x}_{i}-\boldsymbol{\mu}\right)'\right]\boldsymbol{\gamma}{}_{j}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}\right)=\Sigma\cdot\boldsymbol{\gamma}{}_{j}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Sigma\Gamma=\Gamma\Lambda$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Sigma\Gamma=\left(\boldsymbol{\gamma}_{i}\right)_{i=1}^{n}\left(\delta_{ij}\lambda_{i}\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)=\left[\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}\right)\right]_{jl}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)=\left[\Sigma\Gamma\right]_{jl}
\]

\end_inset


\end_layout

\begin_layout Standard
El elemento de ua matriz producto es un propducto fila por columna, por
 la forma de las columnas de 
\begin_inset Formula $\Lambda$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{\gamma}_{l;j}$
\end_inset

 es la coordenada 
\begin_inset Formula $j$
\end_inset

 de cada uno de los 
\begin_inset Formula $\boldsymbol{\gamma}$
\end_inset

, 
\begin_inset Formula $\boldsymbol{\gamma}_{l;j}=\left[\left(\boldsymbol{\gamma}_{l}\right)_{j}\right]_{l=1}^{p}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)=\boldsymbol{\gamma}_{l;j}\lambda_{l}
\]

\end_inset


\end_layout

\begin_layout Standard
entonces
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\rho\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)=\frac{\boldsymbol{\gamma}_{l;j}\lambda_{l}}{\sqrt{\sigma_{jj}\lambda_{l}}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\rho\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)=\boldsymbol{\gamma}_{l;j}\sqrt{\frac{\lambda_{l}}{\sigma_{jj}}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\rho^{2}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)\sigma_{jj}=\boldsymbol{\gamma}_{l;j}^{2}\lambda_{l}
\]

\end_inset


\end_layout

\begin_layout Section
Predicción
\end_layout

\begin_layout Subsection
¿Cómo predigo un vector 
\begin_inset Formula $\boldsymbol{x}$
\end_inset

 en base a otro de dimensión inferior (el de las primeras componentes principale
s)?
\end_layout

\begin_layout Standard
Usó directamente el mejor predictor lineal.
\end_layout

\begin_layout Subsection
En qué sentido es óptimo
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{u}=\boldsymbol{x}-\boldsymbol{x}^{*}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{x}^{*}=\boldsymbol{\mu}+\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}_{q}\right)\text{Var}^{-1}\left(\boldsymbol{v}_{q}\right)\boldsymbol{v}_{q}=\boldsymbol{\mu}+\Gamma_{q}\boldsymbol{v}_{q}$
\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula $\boldsymbol{\mu}+\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}_{q}\right)\text{Var}^{-1}\left(\boldsymbol{v}_{q}\right)\boldsymbol{v}_{q}=\boldsymbol{\mu}+\Gamma_{q}\boldsymbol{v}_{q}$
\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}_{q}\right)=\mathbb{E}\left[\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\left(\boldsymbol{x}-\boldsymbol{\mu}\right)'\right]\Gamma_{q}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}_{q}\right)=\Sigma\Gamma_{q}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}_{q}\right)=\Gamma_{q}\Lambda_{q}
\]

\end_inset


\end_layout

\begin_layout Standard
por ortogonalidad a los posterires
\end_layout

\begin_layout Standard
Además 
\begin_inset Formula $\text{Var}\left(\boldsymbol{v}_{q}\right)=\Lambda_{q}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}^{*}=\boldsymbol{\mu}+\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}_{q}\right)\text{Var}^{-1}\left(\boldsymbol{v}_{q}\right)\boldsymbol{v}_{q}=\boldsymbol{\mu}+\Gamma_{q}\Lambda_{q}\Lambda_{q}^{-1}\boldsymbol{v}_{q}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}^{*}=\boldsymbol{\mu}+\text{Cov}\left(\boldsymbol{x};\boldsymbol{v}_{q}\right)\text{Var}^{-1}\left(\boldsymbol{v}_{q}\right)\boldsymbol{v}_{q}=\boldsymbol{\mu}+\Gamma_{q}\boldsymbol{v}_{q}
\]

\end_inset


\end_layout

\begin_layout Subsection
Probamos que es el mejor estimador
\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{u}=\boldsymbol{x}-\boldsymbol{x}^{*}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\lVert\boldsymbol{u}\rVert^{2}=\mathbb{E}\left(\sum_{i=1}^{p}\boldsymbol{u}_{j}^{2}\right)=\left(\sum_{i=1}^{p}\mathbb{E}\boldsymbol{u}_{j}^{2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\lVert\boldsymbol{u}\rVert^{2}=\text{Var}\left(\boldsymbol{u}_{j}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\lVert\boldsymbol{u}\rVert^{2}=\text{tr}\left[\text{Var}\left(\boldsymbol{u}\right)\right]
\]

\end_inset


\end_layout

\begin_layout Standard
Y
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}\right)=\text{Var}\left(\boldsymbol{x}\right)-\text{Var}\left(\boldsymbol{x}^{*}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Este resultado es automático porque sale del hecho de que este es el mejor
 predictor lineal.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}\right)=\text{Cov}\left(\boldsymbol{x}-\boldsymbol{x}^{*};\boldsymbol{x}-\boldsymbol{x}^{*}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}\right)=\text{Var}\left(\boldsymbol{x}\right)+\text{Var}\left(\boldsymbol{x}^{*}\right)-\text{Cov}\left(\boldsymbol{x};\boldsymbol{x}^{*}\right)-\text{Cov}\left(\boldsymbol{x}^{*};\boldsymbol{x}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{Var}\left(\boldsymbol{x}^{*}\right)=\text{Var}\left(\boldsymbol{\mu}+\Gamma_{q}\boldsymbol{v}_{q}\right)=\text{Var}\left(\Gamma_{q}\boldsymbol{v}_{q}\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{Var}\left(\boldsymbol{x}^{*}\right)=\Gamma_{q}\text{Var}\left(\boldsymbol{v}_{q}\right)\Gamma_{q}'$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{Var}\left(\boldsymbol{x}^{*}\right)=\Gamma_{q}\Lambda_{q}\Gamma_{q}'=\sum_{j=1}^{q}\boldsymbol{\gamma}_{j}\boldsymbol{\gamma}_{j}'\lambda_{j}$
\end_inset

, es decir que no conserva las buenas propiedades de 
\begin_inset Formula $\Gamma\Lambda$
\end_inset

.
\end_layout

\begin_layout Standard
Además
\end_layout

\begin_layout Standard
\begin_inset Formula $\text{Cov}\left(\boldsymbol{x};\boldsymbol{x}^{*}\right)=\mathbb{E}\left[\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\boldsymbol{v}'_{q}\Gamma'_{q}\right]$
\end_inset

 def 
\begin_inset Formula $\boldsymbol{v}_{q}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{Cov}\left(\boldsymbol{x};\boldsymbol{x}^{*}\right)=\mathbb{E}\left[\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\left(\boldsymbol{x}-\boldsymbol{\mu}\right)'\right]\Gamma{}_{q}\Gamma'_{q}$
\end_inset

 
\end_layout

\begin_layout Standard
\begin_inset Formula $\text{Cov}\left(\boldsymbol{x};\boldsymbol{x}^{*}\right)=\underbrace{\Sigma\Gamma{}_{q}}_{\Gamma_{q}\Lambda_{q}}\Gamma'_{q}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{Cov}\left(\boldsymbol{x};\boldsymbol{x}^{*}\right)=\Gamma_{q}\Lambda_{q}\Gamma'_{q}$
\end_inset

 y como es simétrica, también es la otra covarianza.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}\right)=\Sigma+\Gamma_{q}\Lambda_{q}\Gamma'_{q}-2\Gamma_{q}\Lambda_{q}\Gamma'_{q}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}\right)=\Sigma-\underbrace{\Gamma_{q}\Lambda_{q}\Gamma'_{q}}_{\text{Var}\left(\boldsymbol{x}^{*}\right)}
\]

\end_inset


\end_layout

\begin_layout Subsection
para una componente
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}_{j}\right)=\left(\Sigma-\Gamma_{q}\Lambda_{q}\Gamma'_{q}\right)_{jj}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}_{j}\right)=\sigma_{jj}-\left(\Gamma_{q}\Lambda_{q}\Gamma'_{q}\right)_{jj}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}_{j}\right)=\sigma_{jj}-\left(\sum_{l=1}^{q}\lambda_{l}\boldsymbol{\gamma}_{l}\boldsymbol{\gamma}_{l}'\right)_{jj}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\sum_{l=1}^{q}\lambda_{l}\boldsymbol{\gamma}_{l}\boldsymbol{\gamma}_{l}'\right)_{jj}=\sum_{l=1}^{q}\lambda_{l}\left[\left(\boldsymbol{\gamma}_{l}\right)_{j}\right]\left[\left(\boldsymbol{\gamma}_{l}\right)_{j}\right]$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\sum_{l=1}^{q}\lambda_{l}\boldsymbol{\gamma}_{l}\boldsymbol{\gamma}_{l}'\right)_{jj}=\sum_{l=1}^{q}\lambda_{l}\left(\boldsymbol{\gamma}_{l}\right)_{j}^{2}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}_{j}\right)=\sigma_{jj}-\sum_{l=1}^{q}\lambda_{l}\left(\boldsymbol{\gamma}_{l}\right)_{j}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
Volvemos a 
\begin_inset Formula $\rho$
\end_inset

 
\begin_inset Formula $\rho\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)=\boldsymbol{\gamma}_{l;j}\sqrt{\frac{\lambda_{l}}{\sigma_{jj}}}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}_{j}\right)=\sigma_{jj}-\sum_{l=1}^{q}\cancel{\lambda_{l}}\frac{\sigma_{jj}\rho^{2}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)}{\cancel{\lambda_{l}}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}_{j}\right)=\sigma_{jj}-\sum_{l=1}^{q}\sigma_{jj}\rho^{2}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{u}_{j}\right)=\sigma_{jj}\left(1-\sum_{l=1}^{q}\rho^{2}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Logramos separar la parte explicada de la varianza por cada 
\begin_inset Formula $\boldsymbol{v}_{l}$
\end_inset

.
 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert\boldsymbol{u}\rVert^{2}\right)=\sum_{j=1}^{p}\sigma_{jj}\left(1-\sum_{l=1}^{q}\rho^{2}\left(\boldsymbol{x}_{j};\boldsymbol{v}_{l}\right)\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Si en vez de usar estas componentes uso cualquier otra CL de 
\begin_inset Formula $\boldsymbol{x}$
\end_inset

 esta esperanza de los residuos va a ser superior siempre.
\end_layout

\begin_layout Itemize
Esta esperanza es sólo mínima para este predictor lineal.
 Esto no lo probamos.
\end_layout

\begin_layout Subsection
Aplicación
\end_layout

\begin_layout Standard
Diapo 15
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Q_{1}=\lVert\boldsymbol{x}-\boldsymbol{x}_{q}^{*}\rVert=\left(\boldsymbol{x}-\boldsymbol{\mu}-\Gamma_{q}\boldsymbol{v}_{q}\right)'\left(\boldsymbol{x}-\boldsymbol{\mu}-\Gamma_{q}\boldsymbol{v}_{q}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Q_{1}=\sum_{j=q+1}^{p}\boldsymbol{v}_{j}^{2}
\]

\end_inset


\end_layout

\begin_layout Proof
El resto se expresa con las restantes CP
\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\lVert\boldsymbol{x}-\boldsymbol{x}_{q}^{*}\rVert=\left(\boldsymbol{x}-\boldsymbol{\mu}-\Gamma_{q}\boldsymbol{v}_{q}\right)'\left(\boldsymbol{x}-\boldsymbol{\mu}-\Gamma_{q}\boldsymbol{v}_{q}\right)
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula $\boldsymbol{x}=\boldsymbol{\mu}+\sum_{j=1}^{p}\boldsymbol{\gamma}_{j}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\boldsymbol{\gamma}_{j}'$
\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula $\boldsymbol{x}=\boldsymbol{\mu}+\sum_{j=1}^{p}\boldsymbol{v}_{j}\boldsymbol{\gamma}_{j}'$
\end_inset


\end_layout

\begin_layout Proof
además
\end_layout

\begin_layout Proof
\begin_inset Formula $\Gamma_{q}\boldsymbol{v}_{q}=\left(\boldsymbol{\gamma}_{i}\right)\left(\begin{matrix}\boldsymbol{v}_{i}\end{matrix}\right)=\sum_{j=1}^{q}\boldsymbol{\gamma}_{j}\boldsymbol{v}_{j}$
\end_inset


\end_layout

\begin_layout Proof
Aplicamos
\end_layout

\begin_layout Proof
\begin_inset Formula $\boldsymbol{x}-\boldsymbol{\mu}-\Gamma_{q}\boldsymbol{v}_{q}=\left(\boldsymbol{\mu}+\sum_{j=1}^{q}\boldsymbol{\gamma}_{j}\boldsymbol{v}_{j}\right)-\boldsymbol{\mu}-\sum_{j=1}^{q}\boldsymbol{\gamma}_{j}\boldsymbol{v}_{j}$
\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula $\boldsymbol{x}-\boldsymbol{\mu}-\Gamma_{q}\boldsymbol{v}_{q}=\sum_{j=q+1}^{p}\boldsymbol{\gamma}_{j}\boldsymbol{v}_{j}$
\end_inset


\end_layout

\begin_layout Proof
por norma como suma de cuadrados de componentes
\end_layout

\begin_layout Proof
\begin_inset Formula $\lVert\boldsymbol{x}-\boldsymbol{\mu}-\Gamma_{q}\boldsymbol{v}_{q}\rVert^{2}=\sum_{j=q+1}^{p}\boldsymbol{v}_{j}^{2}$
\end_inset


\end_layout

\begin_layout Proof
probado
\end_layout

\begin_layout Proof
ADemás
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(Q_{1}\right)=\mathbb{E}\left(\sum_{j=q+1}^{p}\boldsymbol{v}_{j}^{2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
esta es su varianza poque estan centrados
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(Q_{1}\right)=\sum_{j=q+1}^{p}\lambda_{j}
\]

\end_inset


\end_layout

\begin_layout Standard
Esto, 
\begin_inset Formula $\mathbb{E}\left(Q_{1}\right)$
\end_inset

,sería una suma de chi cuadrados pesadaporque cada una tiene otra varianza.
\end_layout

\begin_layout Standard
se aproxima por una normal.
\end_layout

\begin_layout Standard
La potencia está para simetrizar, es similar a la transformación de Box
 y Cox.
\end_layout

\begin_layout Standard
Los
\begin_inset Formula $\theta_{n}$
\end_inset

 son suma de autovalores con potencia 
\begin_inset Formula $n$
\end_inset

 en la diapo 16.
\end_layout

\begin_layout Standard
LA razón 
\begin_inset Formula $\frac{Q_{1}}{\sum_{j=q+1}^{p}\lambda_{j}}$
\end_inset

 mide la relación entre valor observado y predicho.
 Con la aproximación por la normal indicados cuándo es muy grande el error.
\end_layout

\begin_layout Subsection
pregunta que no escuché
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Q_{1}=\sum_{j=q+1}^{p}\lambda_{j}\boldsymbol{z}_{j}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
Si 
\begin_inset Formula $\boldsymbol{x}\sim\mathcal{N}\left(\boldsymbol{\mu};\Sigma\right)$
\end_inset

 entonces 
\begin_inset Formula $\boldsymbol{z}\sim\mathcal{N}\left(\boldsymbol{0};I\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Q_{1}=\sum_{j=q+1}^{p}\lambda_{j}\underbrace{\chi_{1;j}^{2}}_{indep}
\]

\end_inset


\end_layout

\begin_layout Section
Lemas Previos
\end_layout

\begin_layout Subsection
Courant Fisher
\end_layout

\begin_layout Standard
Al agregar el ínfimo para B, me obliga a sacar los autovaores.
\end_layout

\begin_layout Standard
Pensando en una base ortogonal, agregar la matriz 
\begin_inset Formula $B$
\end_inset

 es como un permiso para tachar los primeros 
\begin_inset Formula $k$
\end_inset

 autovectores de mayor autovalor para achicar el argumento.
\end_layout

\begin_layout Standard
Se puede extender a dimensión infinita.
\end_layout

\begin_layout Subsection
Poincaré
\end_layout

\begin_layout Itemize
en el tercero, 
\begin_inset Formula $s=j$
\end_inset

.
\end_layout

\begin_layout Section
Optimalidad
\end_layout

\begin_layout Standard
Elegimos esperanza 0 para hacer más sencillas las demostraciones.
\end_layout

\begin_layout Standard
La desigualdad de autovalores es para poder identificar los autovectores.
\end_layout

\begin_layout Standard
En reaidad, vale una prpiedad más fuerte si ponemos más condicines.
 
\end_layout

\begin_layout Standard
Si 
\begin_inset Formula $\boldsymbol{x}$
\end_inset

 tiene distribución elíptica y quitamos la esperanza, la variable aleatoria
 de una de las distancias es estocásticamente menor
\begin_inset Foot
status open

\begin_layout Plain Layout
\begin_inset Formula $U\overset{es}{\leq}V\Leftrightarrow\forall a:F_{U}\left(a\right)\geq F_{V}\left(a\right)$
\end_inset


\end_layout

\end_inset

 que la otra (no la esperanza).
\end_layout

\begin_layout Standard
Como la norma es invariante por transformacinoes ortogonales, sin pérdida
 de generalidad puedo suponer que 
\begin_inset Formula $\Sigma$
\end_inset

 es diagonal
\begin_inset Foot
status open

\begin_layout Plain Layout
si no lo fuera, la puedo diagonalizar por una transformación ortogonal
\end_layout

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\Sigma=\sum_{i=1}^{p}\delta_{ij}\lambda_{i}
\]

\end_inset


\end_layout

\begin_layout Subsection
Esperanza de la proyección, 
\begin_inset Quotes fld
\end_inset

propiead geométrica
\begin_inset Quotes frd
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{H}_{0}=\left\langle \boldsymbol{\gamma}_{i}\right\rangle _{i=1}^{q}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\underbrace{\pi\left(\boldsymbol{x};\mathcal{H}_{0}\right)}_{\boldsymbol{x}^{*}}\rVert^{2}\right)=\sum_{i=q+1}^{p}\lambda_{i}
\]

\end_inset


\end_layout

\begin_layout Standard
Sea 
\begin_inset Formula $\mathcal{H}=\left\langle \boldsymbol{w}_{i}\right\rangle _{i=1}^{p-q}$
\end_inset

, una base ortogonal.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}-\pi\left(\boldsymbol{x};\mathcal{H}\right)=\pi\left(\boldsymbol{x};\mathcal{H}^{\perp}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
dado que elegimos la base ortogonal
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}-\pi\left(\boldsymbol{x};\mathcal{H}\right)=WW'\boldsymbol{x}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\mathbb{E}\left[\left(x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\right)'\left(x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\right)\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\mathbb{E}\left[\boldsymbol{x}'W'W\underbrace{WW'}_{I_{p-q}}\boldsymbol{x}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\mathbb{E}\left[\underbrace{\boldsymbol{x}'W'W\boldsymbol{x}}_{\mathbb{R}}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\mathbb{E}\left[\text{tr}\left(\boldsymbol{x}'W'W\boldsymbol{x}\right)\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\mathbb{E}\left[\text{tr}\left(WW'\boldsymbol{x}\boldsymbol{x}'\right)\right]
\]

\end_inset


\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\mathbb{E}\left[\text{tr}\left(W'\boldsymbol{x}\boldsymbol{x}'W\right)\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\text{tr}\left[\mathbb{E}\left(W'\boldsymbol{x}\boldsymbol{x}'W\right)\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\text{tr}\left[W'\mathbb{E}\left(\boldsymbol{x}\boldsymbol{x}'\right)W\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\text{tr}\left[W'\Sigma W\right]
\]

\end_inset


\end_layout

\begin_layout Standard
la traza de una matriz es la suma de sus autovalores
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\sum_{j=1}^{p-q}\lambda_{j}\left(W'\Sigma W\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\sum_{j=1}^{p-q}\lambda_{j}\left(W'\Sigma W\right)\geq\sum_{j=q+1}^{p}\lambda_{j}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\lVert x-\pi\left(\boldsymbol{x};\mathcal{H}\right)\rVert^{2}\right)=\sum_{j=1}^{p-q}\lambda_{j}\left(W'\Sigma W\right)\geq\sum_{j=1}^{p-q}\lambda_{j+q}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
Aplico Poincaré.
\end_layout

\begin_layout Itemize
Si fuea elíptica puedo sacar la esperanza y es puramente geométrico.
\end_layout

\begin_layout Subsection
Propiedad 2a
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{a}'\boldsymbol{x}\right)=\boldsymbol{a}'\Sigma\boldsymbol{a}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\max_{\lVert\boldsymbol{a}\rVert=1}\text{Var}\left(\boldsymbol{a}'\boldsymbol{x}\right)=\max_{\lVert\boldsymbol{a}\rVert=1}\boldsymbol{a}'\Sigma\boldsymbol{a}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\max_{\lVert\boldsymbol{a}\rVert=1}\text{Var}\left(\boldsymbol{a}'\boldsymbol{x}\right)=\lambda_{1}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\max_{\lVert\boldsymbol{a}\rVert=1}\text{Var}\left(\boldsymbol{a}'\boldsymbol{x}\right)=\boldsymbol{\gamma}_{1}\Sigma\boldsymbol{\gamma}_{1}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\max_{\lVert\boldsymbol{a}\rVert=1}\text{Var}\left(\boldsymbol{a}'\boldsymbol{x}\right)=\text{Var}\left(\boldsymbol{v}_{1}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\max_{\lVert\boldsymbol{a}\rVert=1}\text{Var}\left(\boldsymbol{a}'\boldsymbol{x}\right)=\text{Var}\left(\boldsymbol{\gamma}'_{1}\boldsymbol{x}\right)
\]

\end_inset


\end_layout

\begin_layout Subsection
Propiedad 2b
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov\left(\boldsymbol{a}'\boldsymbol{x;}\boldsymbol{v}_{j}\right)}=\text{Cov}\left(\boldsymbol{a}'\boldsymbol{x};\boldsymbol{\gamma}'_{j}\boldsymbol{x}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov\left(\boldsymbol{a}'\boldsymbol{x;}\boldsymbol{v}_{j}\right)}=\text{Cov}\left(\boldsymbol{a}'\left(\boldsymbol{x}-\boldsymbol{\mu}\right);\boldsymbol{\gamma}'_{j}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov\left(\boldsymbol{a}'\boldsymbol{x;}\boldsymbol{v}_{j}\right)}=\mathbb{E}\left[\boldsymbol{a}'\left(\boldsymbol{x}-\boldsymbol{\mu}\right)\left(\boldsymbol{x}-\boldsymbol{\mu}\right)'\boldsymbol{\gamma}{}_{j}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov\left(\boldsymbol{a}'\boldsymbol{x;}\boldsymbol{v}_{j}\right)}=\boldsymbol{a}'\Sigma\boldsymbol{\gamma}_{j}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov\left(\boldsymbol{a}'\boldsymbol{x;}\boldsymbol{v}_{j}\right)}=\lambda_{j}\boldsymbol{a}'\boldsymbol{\gamma}_{j}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov\left(\boldsymbol{a}'\boldsymbol{x;}\boldsymbol{v}_{j}\right)}=0\Leftrightarrow\boldsymbol{a}'\boldsymbol{\gamma}_{j}=0
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{\ensuremath{\overset{\max}{\boldsymbol{a}/\text{Cov}\left(\boldsymbol{a};\boldsymbol{v}_{1}\right)=0}}}\boldsymbol{a}'\Sigma\boldsymbol{a}=\text{\ensuremath{\overset{\max}{\boldsymbol{a}/\boldsymbol{a}'\boldsymbol{\gamma}_{j}=0j<k=0}}}\boldsymbol{a}'\Sigma\boldsymbol{a}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{\ensuremath{\overset{\max}{\boldsymbol{a}/\text{Cov}\left(\boldsymbol{a};\boldsymbol{v}_{1}\right)=0}}}\boldsymbol{a}'\Sigma\boldsymbol{a}=\text{\ensuremath{\overset{\max}{\boldsymbol{a}/\boldsymbol{a}'\boldsymbol{\gamma}_{j}=0;j<k=\lambda_{k+1}}}}
\]

\end_inset


\end_layout

\begin_layout Standard
y se alcanza en 
\begin_inset Formula $\boldsymbol{\gamma}'_{k+1}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)$
\end_inset

.
\end_layout

\begin_layout Definition
Variabilidad total
\end_layout

\begin_layout Definition
\begin_inset Formula $\text{tr}\Sigma$
\end_inset


\end_layout

\begin_layout Subsection
Encontramos la variabilidad absorvida con la razón de traza.
\end_layout

\begin_layout Standard

\end_layout

\end_body
\end_document
