#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass amsart
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language spanish-mexico
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style french
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Repaso AL
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{tr}\left(A\sum_{i=1}^{n}\boldsymbol{x}_{i}\boldsymbol{x}_{i}'\right)=\sum_{i=1}^{n}\text{tr}\left(A\left[\boldsymbol{x}_{i}\boldsymbol{x}_{i}'\right]\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{tr}\left(A\sum_{i=1}^{n}\boldsymbol{x}_{i}\boldsymbol{x}_{i}'\right)=\sum_{i=1}^{n}\text{tr}\left(\left[\boldsymbol{x}_{i}\boldsymbol{x}_{i}'\right]A\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{tr}\left(A\sum_{i=1}^{n}\boldsymbol{x}_{i}\boldsymbol{x}_{i}'\right)=\sum_{i=1}^{n}\text{tr}\left(\boldsymbol{x}_{i}\left[\boldsymbol{x}_{i}'A\right]\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{tr}\left(A\sum_{i=1}^{n}\boldsymbol{x}_{i}\boldsymbol{x}_{i}'\right)=\sum_{i=1}^{n}\text{tr}\left(\boldsymbol{x}_{i}'A\boldsymbol{x}_{i}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{tr}\left(A\sum_{i=1}^{n}\boldsymbol{x}_{i}\boldsymbol{x}_{i}'\right)=\text{tr}\left(\boldsymbol{x}_{i}'A\boldsymbol{x}_{i}\right)
\]

\end_inset


\end_layout

\begin_layout Section
derivadas 
\end_layout

\begin_layout Standard
\begin_inset Formula $\frac{\partial}{\partial\boldsymbol{x}}\left(\boldsymbol{x}'A\boldsymbol{x}\right)=2A\boldsymbol{x}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\frac{\partial}{\partial A}\left(\boldsymbol{x}'A\boldsymbol{x}\right)=\boldsymbol{x}\boldsymbol{x}'$
\end_inset

, esto en principio es intuitivo porque es la forma que conserva una matriz.
\end_layout

\begin_layout Standard
Para probarlo alcanza con realizar la derivada componente a componente y
 reorganizar los coeficientes un poco.
 
\end_layout

\begin_layout Proof
\begin_inset Formula $\frac{\partial}{\partial A}\text{det}\left(A\right)=\text{det}\left(A\right)A^{-1}$
\end_inset

.
\end_layout

\begin_layout Proof
Consideramos el determinante del elemento 
\begin_inset Formula $a_{ij}$
\end_inset

: para esto necesitamos el cofactor, 
\begin_inset Formula $A_{ij}$
\end_inset

.
 Desarrollamos por una fila.
\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\left|A\right|=\sum_{i=1}^{p}a_{ij}\cdot A_{ij}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\left(\frac{\partial}{\partial A}\left|A\right|\right)_{ij}=A_{ij}
\]

\end_inset


\end_layout

\begin_layout Proof
Es decir que la derivada es la suma de los cofactores.
 Estos pueden expresarse usando la matriz inversa dado que son sus componentes.
\end_layout

\begin_layout Proof
Consideramos la inversa 
\begin_inset Formula $A^{-1}=\left(a^{ij}\right)$
\end_inset

, entonces 
\begin_inset Formula $a^{ij}=\frac{A_{ij}}{\left|A\right|}$
\end_inset

.
\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\left|A\right|a^{ij}=A_{ij}
\]

\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\left(\frac{\partial}{\partial A}\left|A\right|\right)_{ij}=\left|A\right|A^{-1}
\]

\end_inset


\end_layout

\begin_layout Section
Expresión de las compontenes de la matriz de covarianza
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
C=\left(\boldsymbol{c}_{ls}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left(\boldsymbol{x}\boldsymbol{y}'\right)=\left(x_{l}x_{s}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{x};\boldsymbol{y}\right)=\mathbb{E}\left(\boldsymbol{x}\boldsymbol{y}'\right)-\mathbb{E}\left(\boldsymbol{x}\right)\mathbb{E}\left(\boldsymbol{y}'\right)
\]

\end_inset


\end_layout

\begin_layout Standard
la varianza es no negativa
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{x}\boldsymbol{x}'\right)-\mathbb{E}\left(\boldsymbol{x}\right)\mathbb{E}\left(\boldsymbol{x}'\right)\geq0
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{a}'\text{Var}\left(\boldsymbol{x}\right)\boldsymbol{a}=\mathbb{E}\left(\boldsymbol{a}'\boldsymbol{x}\right)\left(\boldsymbol{x}'\boldsymbol{a}\right)-\boldsymbol{a}'\mathbb{E}\left(\boldsymbol{x}\right)\mathbb{E}'\left(\boldsymbol{x}\right)\boldsymbol{a}
\]

\end_inset


\end_layout

\begin_layout Standard
Ver desigualdad e jensen
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{a}'\text{Var}\left(\boldsymbol{x}\right)\boldsymbol{a}=\mathbb{E}^{2}\left(\boldsymbol{a}'\boldsymbol{x}\right)-\left[\mathbb{E}'\left(\boldsymbol{x}\right)\boldsymbol{a}\right]^{2}
\]

\end_inset


\end_layout

\begin_layout Section
Covarianza de la matriz entera
\end_layout

\begin_layout Standard
Como las filas son independientes entre sí, las submatrices no diagonales
 son nulas.
\end_layout

\begin_layout Standard
En la cada submatriz diagonal aparece 
\begin_inset Formula $\Sigma$
\end_inset

.
\end_layout

\begin_layout Section
Propiedad de la esperanza
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(X'AX\right)=\sum_{i=1}^{n}a_{ii}\Sigma_{ii}+\mathbb{E}'\left(X\right)A\mathbb{E}\left(X\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbb{E}\left(X'AX\right)=\mathbb{E}\left(\sum_{i;j}a_{ij}x_{i}x_{j}'\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbb{E}\left(X'AX\right)=\sum_{i;j}\mathbb{E}\left(a_{ij}x_{i}x_{j}'\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $Cov\left(x_{i};x_{j}\right)=\begin{cases}
0 & i\neq j\\
\Sigma_{i} & i=j
\end{cases}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(x_{i};x_{j}\right)=\mathbb{E}\left(x_{i}x_{j}'\right)-\mathbb{E}\left(x_{i}\right)\mathbb{E}'\left(x_{j}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(x_{i};x_{j}\right)=\mathbb{E}\left(x_{i}x_{j}'\right)-\mu_{i}\mu_{j}'
\]

\end_inset


\end_layout

\begin_layout Standard
entonces
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\sum_{ij}a_{ij}\delta_{ij}\Sigma_{i}+\underbrace{\sum_{ij}a_{ij}\mu_{i}\mu_{j}'}_{\left(\mathbb{E}X\right)'A\left(\mathbb{E}X\right)}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\sum_{i}a_{ii}\Sigma_{i}+\left(\mathbb{E}X\right)'A\left(\mathbb{E}X\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{tr}\left(A\right)\Sigma+\left(\mathbb{E}X\right)'A\left(\mathbb{E}X\right)
\]

\end_inset


\end_layout

\begin_layout Section
Estimadores
\end_layout

\begin_layout Subsection
Distribución empírica
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
F_{n}\left(x\right)=\frac{1}{n}\sum_{i=1}^{n}\delta_{x_{i}<x}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\approx\mathbb{E}_{F_{n}}
\]

\end_inset


\end_layout

\begin_layout Standard
Una forma de explicar la fórmula del estimador sesgado de la varianza es
 como una varianza basada en la esperanza empírica, que converge a la del
 PGD.
\end_layout

\begin_layout Standard
\begin_inset Formula $A=\left(I-\frac{1}{n}\boldsymbol{1}_{n}\boldsymbol{1}_{n}'\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{S}\right)=\frac{1}{n}\mathbb{E}\left(X'AX\right)
\]

\end_inset


\end_layout

\begin_layout Standard
propiedad de la forma cuadrática
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{S}\right)=\frac{1}{n-1}\left\{ \text{tr}\left(A\right)\Sigma+\left(\mathbb{E}X\right)'A\left(\mathbb{E}X\right)\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{S}\right)=\frac{1}{n-1}\left\{ \text{tr}\left(A\right)\Sigma+\left(\mathbb{E}X\right)'A\left(\mathbb{E}X\right)\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $A\boldsymbol{x}=\boldsymbol{x}-\boldsymbol{1}_{n}\overline{\boldsymbol{x}}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $A\boldsymbol{\mu}=\boldsymbol{1}_{n}\overline{\boldsymbol{x}}-\boldsymbol{1}_{n}\overline{\boldsymbol{x}}=0$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{S}\right)=\frac{1}{n-1}\left\{ \text{tr}\left(A\right)\Sigma+\boldsymbol{0}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\text{tr}A=\text{rg}A=n-\text{rg}\left(\boldsymbol{1}_{n}\boldsymbol{1}_{n}'\right)=n-1$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{S}\right)=\frac{1}{n-1}\left(n-1\right)\Sigma
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(\boldsymbol{S}\right)=\Sigma
\]

\end_inset


\end_layout

\begin_layout Section
característica
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(e^{it\left(x+y\right)}\right)=\mathbb{E}\left(e^{itx}e^{ity}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
si son indep
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(e^{it\left(x+y\right)}\right)=\mathbb{E}\left(e^{itx}\right)\mathbb{E}\left(e^{ity}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbb{E}\left(e^{it\left(x+y\right)}\right)=\varphi_{x}\varphi_{y}
\]

\end_inset


\end_layout

\begin_layout Section
Normal Multivariada
\end_layout

\begin_layout Standard
Otra expresión
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x}\right)=\frac{1}{\left(2\pi\right)^{\nicefrac{1}{2}}}\frac{1}{\left(\prod_{i=1}^{n}\lambda_{i}\right)^{\nicefrac{1}{2}}}\prod_{i=1}^{n}e^{-\frac{1}{2}\frac{x_{i}^{2}}{\lambda_{i}}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x}\right)=\prod_{i=1}^{n}\frac{1}{\left(2\pi\right)^{\nicefrac{1}{2}}}\frac{e^{-\frac{1}{2}\frac{x_{i}^{2}}{\lambda_{i}}}}{\left(\lambda_{i}\right)^{\nicefrac{1}{2}}}
\]

\end_inset


\end_layout

\begin_layout Standard
Como se ve, podemos factorizar las componentes normales, propiedad de las
 distribuciones independientes.
\end_layout

\begin_layout Section
característica de la normal multivariada
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
X\sim\mathcal{N}\left(\boldsymbol{0};I_{p}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
las componentes son iid
\end_layout

\begin_layout Standard
ya vimos que en este caso factorizan
\end_layout

\begin_layout Standard
aplicamos la propiedad de las independientes.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
X\sim\mathcal{N}\left(\boldsymbol{\mu};\Sigma\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
X-\boldsymbol{\mu}\sim\mathcal{N}\left(\boldsymbol{0};\Sigma\right)
\]

\end_inset


\end_layout

\begin_layout Standard
cholesky, 
\begin_inset Formula $\Sigma>0$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
X-\boldsymbol{\mu}\sim\mathcal{N}\left(\boldsymbol{0};CC'\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
C^{-1}\left(X-\boldsymbol{\mu}\right)\sim\mathcal{N}\left(\boldsymbol{0};I_{p}'\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $z=C^{-1}\left(X-\boldsymbol{\mu}\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $C\boldsymbol{Z}+\boldsymbol{\mu}=\boldsymbol{X}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{X}}\left(\boldsymbol{t}\right)=\mathbb{E}\left(e^{i\cdot\left(\boldsymbol{t}'C\boldsymbol{z}\right)+i\cdot\boldsymbol{t}'\boldsymbol{\mu}}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{X}}\left(\boldsymbol{t}\right)=\mathbb{E}\left(e^{i\cdot\left(\boldsymbol{t}'C\boldsymbol{z}\right)}\right)\cdot\underbrace{e^{i\cdot\boldsymbol{t}'\boldsymbol{\mu}}}_{\in\mathbb{R}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{X}}\left(\boldsymbol{t}\right)=\mathbb{E}\left(e^{i\cdot\left(C'\boldsymbol{t}'\right)\boldsymbol{z}}\right)\cdot e^{i\cdot\boldsymbol{t}'\boldsymbol{\mu}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{X}}\left(\boldsymbol{t}\right)=\varphi_{\boldsymbol{Z}}\left(C'\boldsymbol{t}\right)\cdot e^{i\cdot\boldsymbol{t}'\boldsymbol{\mu}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{X}}\left(\boldsymbol{t}\right)=e^{i\cdot\boldsymbol{t}'\boldsymbol{\mu}}\cdot e^{-\frac{1}{2}\boldsymbol{t}'CC'\boldsymbol{t}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{X}}\left(\boldsymbol{t}\right)=e^{i\cdot\boldsymbol{t}'\boldsymbol{\mu}}\cdot e^{-\frac{1}{2}\boldsymbol{t}'\Sigma\boldsymbol{t}}
\]

\end_inset


\end_layout

\begin_layout Standard
Notar que no aparece la inversa de 
\begin_inset Formula $\Sigma$
\end_inset

, uno podría definir la normal a partir de la función característica generalizan
do a 
\begin_inset Formula $\Sigma$
\end_inset

 singular.
\end_layout

\begin_layout Subsection
definición 2 sii 
\begin_inset Formula $\forall\boldsymbol{t}:\boldsymbol{t}'\boldsymbol{X}$
\end_inset

 es normal univariada.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{t}'\boldsymbol{x}}\left(\boldsymbol{s}\right)=\mathbb{E}\left(e^{i\boldsymbol{s}\left(\boldsymbol{t}'\boldsymbol{x}\right)}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{t}'\boldsymbol{x}}\left(\boldsymbol{s}\right)=\mathbb{E}\left(e^{i\left(\left(\boldsymbol{st}\right)'\boldsymbol{x}\right)}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{t}'\boldsymbol{x}}\left(\boldsymbol{s}\right)=\varphi_{\boldsymbol{X}}\left(\boldsymbol{st}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{t}'\boldsymbol{x}}\left(\boldsymbol{s}\right)=e^{i\boldsymbol{s}\boldsymbol{t}'\boldsymbol{\mu}}e^{-\frac{1}{2}\boldsymbol{s}^{2}\boldsymbol{t}'\Sigma\boldsymbol{t}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{t}'\boldsymbol{x}\sim\mathcal{N}\left(\boldsymbol{t}'\boldsymbol{\mu};\boldsymbol{t}'\Sigma\boldsymbol{t}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
También vale la recíproca, si para todo 
\begin_inset Formula $t$
\end_inset

 es normal univariada, entonces es normal multivariada.
\end_layout

\begin_layout Standard
Alcanza con usar la propiedad de evauar en 1, la propiedad que mostró antes
 en la guía.
\end_layout

\begin_layout Standard
Usando esto, para los 
\begin_inset Formula $\boldsymbol{t}$
\end_inset

 vectores canónicos
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{e}_{j}'\boldsymbol{x}\sim\mathcal{N}\left(\boldsymbol{e}_{j}'\boldsymbol{\mu};\boldsymbol{e}_{j}'\Sigma\boldsymbol{e}_{j}\right)\Rightarrow\mathbb{E}\left(\boldsymbol{x}_{j}\right)=\boldsymbol{\mu}_{j}\wedge\text{Var}\left(\boldsymbol{x}_{j}\right)=\sigma_{jj}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{t}=\boldsymbol{e}_{j}+\boldsymbol{e}_{l}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{t}'\boldsymbol{x}\sim\mathcal{N}\left(\boldsymbol{t}'\boldsymbol{\mu};\boldsymbol{t}'\Sigma\boldsymbol{t}\right)\Rightarrow\mathbb{E}\left(\boldsymbol{t}'\boldsymbol{x}\right)=\boldsymbol{\mu}_{j}+\boldsymbol{\mu}_{l}\wedge\boldsymbol{t}\Sigma\boldsymbol{t}=\sigma_{jj}+2\sigma_{jl}+\sigma_{ll}
\]

\end_inset


\end_layout

\begin_layout Standard
Como
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Var}\left(\boldsymbol{x}_{j}+\boldsymbol{x}_{l}\right)=\text{Var}\left(\boldsymbol{x}_{j}\right)+\text{Var}\left(\boldsymbol{x}_{l}\right)+2\text{Cov}\left(\boldsymbol{x}_{j};\boldsymbol{x}_{l}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
aplicando lo recién probado
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\sigma_{jj}+2\sigma_{jl}+\sigma_{ll}=\sigma_{jj}+\sigma_{ll}+2\text{Cov}\left(\boldsymbol{x}_{j};\boldsymbol{x}_{l}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\sigma_{jl}=\text{Cov}\left(\boldsymbol{x}_{j};\boldsymbol{x}_{l}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
etonces
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\text{Cov}\left(\boldsymbol{X};\boldsymbol{X}\right)=\Sigma
\]

\end_inset


\end_layout

\begin_layout Standard
Como en caso de independencia las covarianzas dan 0, 
\begin_inset Formula $\Sigma$
\end_inset

 es diagonal.
\end_layout

\begin_layout Subsection
tenemos la tercera definición, usando la transformada de una normal estándar
\end_layout

\begin_layout Standard
sólo equivalen las tres para 
\begin_inset Formula $\Sigma$
\end_inset

 definida positivay simétrica.
\end_layout

\begin_layout Section
Contraejemplo de componentes nomales estándar
\end_layout

\begin_layout Standard
Está en las diapos.
 
\end_layout

\begin_layout Standard
La normalidad de las marginales no es suficiente.
\end_layout

\end_body
\end_document
