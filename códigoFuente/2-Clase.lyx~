#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass amsart
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language spanish-mexico
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style french
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Clase 2
\end_layout

\begin_layout Section
Más normal multivariada
\end_layout

\begin_layout Proof
\begin_inset Formula $\boldsymbol{x}$
\end_inset

 es normal multivariada sii 
\begin_inset Formula $\boldsymbol{y}=A\boldsymbol{x}$
\end_inset

 lo es
\begin_inset Foot
status open

\begin_layout Plain Layout
Diapositiva 5, remarcada en rosa.
\end_layout

\end_inset

.
 
\end_layout

\begin_layout Standard
Probamos las propiedades de 
\begin_inset Formula $\boldsymbol{y}=A\boldsymbol{x}$
\end_inset

 para el vector normal 
\begin_inset Formula $\boldsymbol{x}\sim\mathcal{N}\left(\boldsymbol{\mu};\Sigma\right)$
\end_inset

 pero con la característica.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\mathbb{E}\left(e^{i\boldsymbol{t}'\boldsymbol{y}}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\mathbb{E}\left(e^{i\boldsymbol{t}'A\boldsymbol{x}}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\mathbb{E}\left(e^{i\left(A'\boldsymbol{t}\right)'\boldsymbol{x}}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\varphi_{\boldsymbol{X}}\left(A'\boldsymbol{t}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Aplicamos la forma conocida de la función característica de la normal.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=e^{i\left(A'\boldsymbol{t}\right)'\boldsymbol{\mu}}e^{-\frac{1}{2}\left(A'\boldsymbol{t}\right)\Sigma\left(A'\boldsymbol{t}\right)'}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=e^{i\boldsymbol{t}'A\boldsymbol{\mu}}e^{-\frac{1}{2}\boldsymbol{t}'\left(A\Sigma A'\right)\boldsymbol{t}}
\]

\end_inset


\end_layout

\begin_layout Standard
Dado que fcar caracteriza una distribución:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{Y}\sim\mathcal{N}\left(A\boldsymbol{\mu};A\Sigma A'\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $A\Sigma A'$
\end_inset

 es definida positiva porque 
\begin_inset Formula $\Sigma>0$
\end_inset

 y 
\begin_inset Formula $A$
\end_inset

 es de rango completo.
\end_layout

\begin_layout Section
Propiedades de Particiones y Transformadas
\begin_inset Foot
status open

\begin_layout Plain Layout
Diapo 60.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Los puntos a) y b) se prueban usando la propiedad precedente y una matriz
 del tipo 
\begin_inset Formula $\left(\begin{matrix}I_{p_{1}} & O\\
O & O
\end{matrix}\right)$
\end_inset

 para el primer bloque.
 Para el b) acudimos al teorema visto en modelo lineal para independencia
 de transformadas de normales, alcanza con el corolario donde la normal
 conjunta de la transformación tiene los bloques diagonales nulos.
 
\end_layout

\begin_layout Standard
El punto c) se probó arriba.
 
\end_layout

\begin_layout Subsection
Otra demo del b)
\end_layout

\begin_layout Standard
PAra b, probamos en la clase pasada la forma de las 
\begin_inset Formula $\Sigma$
\end_inset

 (que sus elementos son covarianzas), que hace inmediata la ida.
\end_layout

\begin_layout Standard
PAra la vuelta, podemos encontrar por la estructura de bloques el 
\begin_inset Formula $\Sigma^{-1}$
\end_inset

 y eso permite factorizar la densidad.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\Sigma=\left(\begin{matrix}\Sigma_{11} & \boldsymbol{0}\\
\boldsymbol{0} & \Sigma_{22}
\end{matrix}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\Sigma^{-1}=\left(\begin{matrix}\Sigma_{11}^{-1} & \boldsymbol{0}\\
\boldsymbol{0} & \Sigma_{22}^{-1}
\end{matrix}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x}\right)=f_{\left(X_{1};X_{2}\right)}\left(x_{1};x_{2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\boldsymbol{X}}\left(\boldsymbol{x}\right)=\frac{1}{\left(2\pi\right)^{\frac{np}{2}}}\frac{1}{\left|\Sigma\right|^{\nicefrac{1}{2}}}e^{-\nicefrac{1}{2}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)'\Sigma^{-1}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\boldsymbol{x}-\boldsymbol{\mu}\right)'\Sigma^{-1}\left(\boldsymbol{x}-\boldsymbol{\mu}\right)=\left(\boldsymbol{x}_{1}-\boldsymbol{\mu}_{1}\right)'\Sigma_{11}^{-1}\left(\boldsymbol{x}_{1}-\boldsymbol{\mu}_{1}\right)+\left(\boldsymbol{x}_{2}-\boldsymbol{\mu}_{2}\right)'\Sigma_{22}^{-1}\left(\boldsymbol{x}_{2}-\boldsymbol{\mu}_{2}\right)$
\end_inset


\end_layout

\begin_layout Standard
Acá se ve fácil que puedo factorizar a dos funcinoes monovariadas y por
 lo tanto las variables son independientes.
\end_layout

\begin_layout Itemize
En particular para la normal vectorial estándar y una ortogonal completa,
 se conserva la distribución normal estándar, ya que en 
\begin_inset Formula $H\cdot\boldsymbol{\mu}=H\cdot\boldsymbol{0}=\boldsymbol{0}=\boldsymbol{\mu}$
\end_inset

 y 
\begin_inset Formula $H'IH=H'H=I$
\end_inset

.
 Esto vale también para una ortonormal incompleta, donde se reduce la dimensión
 de la distribución.
\end_layout

\begin_layout Definition
Distribución esférica
\end_layout

\begin_layout Definition
Es invariante para transformaciones ortogonales.
\end_layout

\begin_layout Definition
Un ejemplo es la uniforme en la esfera, que se puede obtener transformando
 el vector normal en versor: 
\begin_inset Formula $\boldsymbol{X}\sim\mathcal{N}\left(\boldsymbol{0};I\right)\Rightarrow\frac{\boldsymbol{X}}{\lVert\boldsymbol{X}\rVert}\sim\mathcal{U}\left(S_{p}\right)$
\end_inset


\begin_inset Foot
status open

\begin_layout Plain Layout
\begin_inset Formula $S_{p}$
\end_inset

 es la esfera en 
\begin_inset Formula $\mathbb{R}^{p}$
\end_inset

.
\end_layout

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Standard
La propiedad 
\begin_inset Formula $e$
\end_inset

 sirve para generar normales de toda clase, generando sólo normales estándar
 escalares y aplicando 
\begin_inset Formula $x_{ij}=Az_{ij}+\mu_{ij}$
\end_inset

.
\end_layout

\begin_layout Standard
La librería que genera normales multivariadas es MVTnorm.
\end_layout

\begin_layout Standard
chol te da la traspuesta de la raíz como la definimos nosotros.
\end_layout

\begin_layout Standard
La descomposición no es única y por lo tanto diversos métodos llevan a diversos
 puntos de la misma distribución en base a la misma muestra de normales
 estándar.
\end_layout

\begin_layout Subsection
Ejercicio 3 de la guía 1.
 Suma de cuadrados de nomales no centradas (prop g, diapo 63).
\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{X}\sim\mathcal{N}_{d}\left(\boldsymbol{\mu};\Sigma\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Sigma=A'A$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{x}=A\boldsymbol{z}+\boldsymbol{\mu}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}'\Sigma^{-1}\boldsymbol{x}=\left(A\boldsymbol{z+}\boldsymbol{\mu}\right)'\Sigma^{-1}\left(A\boldsymbol{z+}\boldsymbol{\mu}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}'\Sigma^{-1}\boldsymbol{x}=\left(A\boldsymbol{z+}\boldsymbol{\mu}\right)'A'^{-1}A^{-1}\left(A\boldsymbol{z+}\boldsymbol{\mu}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}'\Sigma^{-1}\boldsymbol{x}=\left(\boldsymbol{z+}A{}^{-1}\boldsymbol{\mu}\right)'\left(\boldsymbol{z}+A^{-1}\boldsymbol{\mu}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}'\Sigma^{-1}\boldsymbol{x}=\lVert\boldsymbol{z}+\underbrace{A^{-1}\boldsymbol{\mu}}_{\boldsymbol{\theta}}\rVert
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{x}'\Sigma^{-1}\boldsymbol{x}=\sum_{i=1}^{p}\left(z_{i}+\theta_{i}\right)^{2}\sim\chi_{p}^{2}\left(\sum_{i=1}^{p}\theta_{i}^{2}\right)
\]

\end_inset


\end_layout

\begin_layout Subsection
Distribución del coeficiente de correlación, está en el libro de Kshirsagar
\end_layout

\begin_layout Standard
Se logra usando las propiedades de las transformaciones ortogonales.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left(\begin{matrix}x_{i}\\
y_{i}
\end{matrix}\right)\sim\mathcal{N}\left(\boldsymbol{\mu};\left(\begin{matrix}\sigma_{11}^{2} & \rho\sigma_{11}\sigma_{22}\\
\rho\sigma_{11}\sigma_{22} & \sigma_{22}^{2}
\end{matrix}\right)\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\hat{\rho}=\frac{\sum_{i}\left(x_{i}-\overline{x}\right)\left(y_{i}-\overline{y}\right)}{\sqrt{\sum_{i}\left(x_{i}-\overline{x}\right)^{2}\sum_{j}\left(y_{j}-\overline{y}\right)^{2}}}
\]

\end_inset


\end_layout

\begin_layout Standard
DEnsidad de 
\begin_inset Formula $\hat{\rho}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{\hat{\rho}}\left(r\right)=\frac{\Gamma\left(\frac{n-1}{2}\right)}{\Gamma\left(\nicefrac{1}{2}\right)\Gamma\left(\frac{n-2}{2}\right)}\left(1-r^{2}\right)^{\frac{n-4}{2}}\cdot\mathbb{I}_{\left|r\right|<1}
\]

\end_inset


\end_layout

\begin_layout Standard
Notar que depende del tamaño de muestra.
\end_layout

\begin_layout Section
Para 
\begin_inset Formula $\boldsymbol{x}\sim\mathcal{N}_{d}\left(\boldsymbol{0};\sigma^{2}\cdot I\right)$
\end_inset

 y una transformación lineal suya 
\begin_inset Formula $\boldsymbol{z}$
\end_inset

, el resto cuadrático 
\begin_inset Formula $\boldsymbol{x}'\boldsymbol{x}-\boldsymbol{z}'\boldsymbol{z}$
\end_inset

 es independiente de 
\begin_inset Formula $\boldsymbol{z}$
\end_inset

 (prop.
 b), diapo 64).
\end_layout

\begin_layout Standard
*****¿Por qué no alcanza con decir que 
\begin_inset Formula $H$
\end_inset

 es una base ortogonal partida y aplicar el teorema para los bloques nulos?
 ¿No estamos limitados por las dimensiones?
\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{z}$
\end_inset

 es independiente de 
\begin_inset Formula $\boldsymbol{x}'\boldsymbol{x}-\boldsymbol{z}'\boldsymbol{z}$
\end_inset

.
\end_layout

\begin_layout Standard
Esta es la propiedad que usamos en modelo lineal para establecer la independenci
a entre 
\begin_inset Formula $S^{2}$
\end_inset

 y 
\begin_inset Formula $\hat{\beta}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{y}=H'\boldsymbol{x}=\left(\begin{matrix}H_{1}'\boldsymbol{x}\\
H_{2}'\boldsymbol{x}
\end{matrix}\right)=\left(\begin{matrix}\boldsymbol{z}\\
\boldsymbol{u}
\end{matrix}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
por ser 
\begin_inset Formula $H$
\end_inset

 ortogonal, 
\begin_inset Formula $H'\boldsymbol{x}\sim\mathcal{N}_{d}\left(\boldsymbol{0};\sigma^{2}HIH'\right)=\mathcal{N}_{d}\left(\boldsymbol{0};\sigma^{2}I\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{y}\sim\mathcal{N}\left(0;\sigma^{2}I\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Entre ambos datos, sabemos que 
\begin_inset Formula $\boldsymbol{z}$
\end_inset

 es independiente de 
\begin_inset Formula $\boldsymbol{u}$
\end_inset

 por la propiedad de los bloques extradiagonales nulos en la matriz de covarianz
as 
\begin_inset Formula $\Sigma$
\end_inset

.
\end_layout

\begin_layout Standard
Como el cuadrado de la norma es la sumatoria de los cuadrados de las componentes
 y 
\begin_inset Formula $\boldsymbol{z}$
\end_inset

 y 
\series bold

\begin_inset Formula $\boldsymbol{u}$
\end_inset


\series default
 parten 
\begin_inset Formula $\boldsymbol{y}$
\end_inset

: 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\lVert\boldsymbol{y}\rVert^{2}=\lVert\boldsymbol{z}\rVert^{2}+\lVert\boldsymbol{u}\rVert^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
Por la construcción de 
\begin_inset Formula $\boldsymbol{y}$
\end_inset

 y de 
\begin_inset Formula $H$
\end_inset

:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\lVert\boldsymbol{y}\rVert^{2}=\lVert H'\boldsymbol{x}\rVert^{2}=\lVert\boldsymbol{x}\rVert^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
entonces
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\lVert\boldsymbol{x}\rVert^{2}=\lVert\boldsymbol{z}\rVert^{2}+\lVert\boldsymbol{u}\rVert^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\lVert\boldsymbol{u}\rVert^{2}=\lVert\boldsymbol{x}\rVert^{2}-\lVert\boldsymbol{z}\rVert^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
Ya mostramos que 
\begin_inset Formula $\boldsymbol{z}$
\end_inset

 es independiente de 
\begin_inset Formula $\boldsymbol{u}$
\end_inset

 y por lo tanto lo es de 
\begin_inset Formula $\lVert\boldsymbol{u}\rVert^{2}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\lVert\boldsymbol{u}\rVert^{2}=\sum u_{i}^{2}=\boldsymbol{x}'\boldsymbol{x}-\boldsymbol{z}'\boldsymbol{z}
\]

\end_inset


\end_layout

\begin_layout Standard
para la c
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{\boldsymbol{x}'\boldsymbol{x}-\boldsymbol{z}'\boldsymbol{z}}{\sigma}=\sum\left(\frac{u_{i}}{\sigma}\right)^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
y estas son normales estándar independientes.
\end_layout

\begin_layout Section
Distribución condicional 
\begin_inset Formula $\boldsymbol{X}_{|\boldsymbol{X}^{\left(2\right)}=\boldsymbol{x}_{0}}^{\left(1\right)}$
\end_inset


\end_layout

\begin_layout Itemize
Copié la demostración en Seber en el apunte 
\begin_inset Quotes fld
\end_inset

Seber-Condicionales de la Normal
\begin_inset Quotes frd
\end_inset

.
 Se apoya en principios distintos.
 Usa el teorema para independencia entre transformaciones lineales de un
 mismo vector normal.
\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{x}=\left(\boldsymbol{x}_{1};\boldsymbol{x}_{2}\right)$
\end_inset

, 
\begin_inset Formula $\boldsymbol{\mu}=\left(\boldsymbol{\mu}_{1};\boldsymbol{\mu}_{2}\right)$
\end_inset

 y 
\begin_inset Formula $\Sigma=\left(\begin{matrix}\Sigma_{11} & \Sigma_{12}\\
\Sigma_{21} & \Sigma_{22}
\end{matrix}\right)$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{X}^{\left(1\right)}-\Sigma_{11}\Sigma_{22}^{-1}\boldsymbol{x}_{0}\bigg|_{\boldsymbol{X}^{\left(2\right)}=\boldsymbol{x}_{0}}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}+\Sigma_{11}\Sigma_{22}^{-1}\boldsymbol{\mu}^{\left(2\right)};\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Como estamos condicionando 
\begin_inset Formula $\boldsymbol{X}^{\left(2\right)}=\boldsymbol{x}_{0}$
\end_inset

, usemos la variable en esta escritura.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{X}^{\left(1\right)}-\Sigma_{11}\Sigma_{22}^{-1}\boldsymbol{x}^{\left(2\right)}\bigg|_{\boldsymbol{X}^{\left(2\right)}=\boldsymbol{x}_{0}}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}+\Sigma_{11}\Sigma_{22}^{-1}\boldsymbol{\mu}^{\left(2\right)};\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Se observa que esta transformación no depende de 
\begin_inset Formula $\boldsymbol{x}^{\left(2\right)}$
\end_inset

.
 La demostración se va a basar en esa propiedad.
\end_layout

\begin_layout Standard
Vamos a concatenar la variable que estamos proponiendo para la distribución
 condicionada de las primeras componentes 
\begin_inset Formula $\boldsymbol{X}^{\left(1\right)}$
\end_inset

 con el vector formado por las componentes restantes.
\end_layout

\begin_layout Standard
Sea 
\begin_inset Formula $\boldsymbol{Y}=A\boldsymbol{X}$
\end_inset

 donde 
\begin_inset Formula $\boldsymbol{Y}^{\left(1\right)}=\boldsymbol{X}^{\left(1\right)}-\Sigma_{11}\Sigma_{22}^{-1}\boldsymbol{X}^{\left(2\right)}$
\end_inset

 y además 
\begin_inset Formula $\boldsymbol{Y}^{\left(2\right)}=\boldsymbol{X}^{\left(2\right)}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
A=\left(\begin{matrix}I_{p_{1}} & \left(\Sigma_{11}\Sigma_{22}^{-1}\right)\\
\boldsymbol{0} & I_{p_{2}}
\end{matrix}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{Y}\sim\mathcal{N}\left(A\boldsymbol{\mu};A\Sigma A'\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $A\boldsymbol{\mu}=\left(\begin{matrix}\boldsymbol{\mu}^{\left(1\right)}-\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{\mu}^{\left(2\right)}\\
\boldsymbol{\mu}^{\left(2\right)}
\end{matrix}\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $A\Sigma A'=\left(\begin{matrix}\overbrace{\Sigma_{11}-\Sigma_{12}\Sigma_{22}^{-1}\Sigma_{21}}^{\Sigma_{11.2}} & \boldsymbol{0}\\
\boldsymbol{0} & \Sigma_{22}
\end{matrix}\right)$
\end_inset


\end_layout

\begin_layout Standard
Vemos la independencia entre los 
\begin_inset Formula $\boldsymbol{Y}^{\left(i\right)}$
\end_inset

 porque los bloques no diagonales son nulos.
 Además conocemos las distribuciones de ambos, en particular de 
\begin_inset Formula $\boldsymbol{Y}^{\left(1\right)}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{Y}^{\left(1\right)}=\boldsymbol{X}^{\left(1\right)}-\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{X}^{\left(2\right)}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}-\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{\mu}^{\left(2\right)};\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Como las 
\begin_inset Formula $\boldsymbol{Y}$
\end_inset


\begin_inset Formula $^{\left(i\right)}$
\end_inset

son independientes, condicionar una por la otra no varía la distribución.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{Y}^{\left(1\right)}\bigg|_{\boldsymbol{Y}\left(2\right)}=\boldsymbol{X}^{\left(1\right)}-\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{X}^{\left(2\right)}\bigg|_{\boldsymbol{Y}^{\left(2\right)}}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}-\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{\mu}^{\left(2\right)};\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Pero esta es una expresión de X
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{V}=\boldsymbol{X}^{\left(1\right)}-\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{X}^{\left(2\right)}\bigg|_{\boldsymbol{Y}^{\left(2\right)}=\boldsymbol{X}^{\left(2\right)}=\boldsymbol{x}_{0}}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}-\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{\mu}^{\left(2\right)};\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{X}^{\left(1\right)}\bigg|_{\boldsymbol{X}^{\left(2\right)}=\boldsymbol{x}_{0}}=\boldsymbol{V}+\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{X}^{\left(2\right)}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}+\Sigma_{12}\Sigma_{22}^{-1}\left(\boldsymbol{X}_{0}-\boldsymbol{\mu}^{\left(2\right)}\right);\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{X}^{\left(1\right)}\bigg|_{\boldsymbol{X}^{\left(2\right)}=\boldsymbol{x}_{0}}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}+\Sigma_{12}\Sigma_{22}^{-1}\left(\boldsymbol{X}_{0}-\boldsymbol{\mu}^{\left(2\right)}\right);\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Subsection
Además, obtenemos un modelo lineal de ciertas variables en función de las
 demás.
\end_layout

\begin_layout Standard
¿Por qué aparece 
\begin_inset Formula $\beta$
\end_inset

?
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{X}^{\left(1\right)}=\boldsymbol{V}+\Sigma_{12}\Sigma_{22}^{-1}\boldsymbol{X}^{\left(2\right)}\bigg|_{\boldsymbol{X}^{\left(2\right)}=\boldsymbol{X}_{0}}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}+\Sigma_{11}\Sigma_{22}^{-1}\left[\boldsymbol{X}_{0}-\boldsymbol{\mu}^{\left(2\right)}\right];\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{X}^{\left(1\right)}=\Sigma_{12}\Sigma_{21}^{-1}\boldsymbol{X}^{\left(2\right)}+\boldsymbol{V}
\]

\end_inset


\end_layout

\begin_layout Standard
ESte 
\begin_inset Formula $\boldsymbol{V}$
\end_inset

 es independiente de 
\begin_inset Formula $\boldsymbol{X}^{\left(2\right)}$
\end_inset

, se porta como un error.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{V}\sim\mathcal{N}\left(\boldsymbol{\mu}^{\left(1\right)}-\Sigma_{11}\Sigma_{22}^{-1}\boldsymbol{\mu}^{\left(2\right)};\Sigma_{11.2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Dado que 
\begin_inset Formula $\boldsymbol{X}^{\left(1\right)}=\Sigma_{12}\Sigma_{21}^{-1}\boldsymbol{X}^{\left(2\right)}+\boldsymbol{V}$
\end_inset

, tenemos el modelo lineal de la suma de las esperanzas más un error:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{X}^{\left(1\right)}=\underbrace{\Sigma_{12}\Sigma_{22}^{-1}}_{\boldsymbol{\beta}}\boldsymbol{X}^{\left(2\right)}+\left[\Sigma_{11}\Sigma_{22}\boldsymbol{\mu}^{\left(2\right)}-\boldsymbol{\mu}^{\left(1\right)}\right]+\boldsymbol{e}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\boldsymbol{e}\sim\mathcal{N}\left(\boldsymbol{0};\Sigma_{11.2}\right)$
\end_inset


\end_layout

\begin_layout Standard
Logramos predecir ciertas coordenadas en función de las restantes, esto
 sirve para los datos faltantes en una muestra.
\end_layout

\begin_layout Standard
Para 
\begin_inset Formula $q=1$
\end_inset

 la matriz degenera a componentes escalares.
 Como 
\begin_inset Formula $\sigma_{11}=\frac{1}{\sigma_{11.2}}$
\end_inset

 podemos despejar ese valor.
\end_layout

\begin_layout Subsection
Producto de vector normal por vector real, p80.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{Y}=\boldsymbol{X}'\boldsymbol{a}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\mathbb{E}\left[e^{i\boldsymbol{t}'\boldsymbol{Y}}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\mathbb{E}\left[e^{i\boldsymbol{t}'\sum_{j=1}^{n}a_{j}x_{j}}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\mathbb{E}\left[\prod_{i=1}^{n}e^{i\boldsymbol{t}'a_{j}x_{j}}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
son indep
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\prod_{i=1}^{n}\mathbb{E}\left[e^{i\boldsymbol{t}'a_{j}x_{j}}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\prod_{i=1}^{n}\varphi_{X_{j}}\left(\boldsymbol{t}'a_{j}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\prod_{i=1}^{n}e^{-\frac{1}{2}\left(\boldsymbol{t}'a_{j}\right)\Sigma\left(\boldsymbol{t}'a_{j}\right)'}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=\prod_{i=1}^{n}e^{-\frac{1}{2}a_{j}^{2}\boldsymbol{t}'\Sigma\boldsymbol{t}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=e^{-\frac{1}{2}\left(\sum a_{j}^{2}\right)\boldsymbol{t}'\Sigma\boldsymbol{t}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varphi_{\boldsymbol{Y}}\left(\boldsymbol{t}\right)=e^{-\frac{1}{2}\boldsymbol{t}'\underbrace{\left(\lVert\boldsymbol{a}\rVert^{2}\Sigma\right)}_{\Sigma^{*}}\boldsymbol{t}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{Y}\sim\mathcal{N}\left(\boldsymbol{0};\Sigma^{*}\right)
\]

\end_inset


\end_layout

\begin_layout Itemize
Hacer lo de los ortogonales.
\end_layout

\begin_layout Standard
Este teorema aplica al cálculo de la media que es una CL.
\end_layout

\begin_layout Standard
Acá 
\begin_inset Formula $\lVert\boldsymbol{a}\rVert^{2}=\sum\frac{1}{n^{2}}=\frac{1}{n}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $S$
\end_inset

 es independiente de 
\begin_inset Formula $\overline{X}$
\end_inset

 porque son parte de una transformación ortogonal, como probamos en proba.
\end_layout

\begin_layout Subsection
Teoremas de la Página 80
\end_layout

\begin_layout Standard
Son los primeros dos ejercicios de la guía.
\end_layout

\begin_layout Section
Wishart y Hotelling
\end_layout

\begin_layout Standard
Queremos encontrar la distribución de la matriz 
\begin_inset Formula $S$
\end_inset

.
\end_layout

\begin_layout Standard
En univariado sabemos que es 
\begin_inset Formula $\chi^{2}=\Gamma\left(\frac{1}{2};\frac{1}{2}\right)$
\end_inset

 y también es la distribución de la suma del cuadrado de normales estándar.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
X/\boldsymbol{x}_{i}\overset{iid}{\sim}\mathcal{N}_{p}\left(\boldsymbol{0};I\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\sum\boldsymbol{x}_{i}\boldsymbol{x}_{i}'
\]

\end_inset


\end_layout

\begin_layout Subsection
Rango de este producto
\end_layout

\begin_layout Standard
\begin_inset Formula $X_{i}X_{i}'$
\end_inset

 tiene rango 1 (es cl de una sola fila) y esta en 
\begin_inset Formula $\mathbb{R}^{p\times p}$
\end_inset

.
 Si sumo 
\begin_inset Formula $n$
\end_inset

 matrices de rango 
\begin_inset Formula $1$
\end_inset

 con 
\begin_inset Formula $n<p$
\end_inset

, la matriz obtenida es singular.
\end_layout

\begin_layout Standard
Para lograr una densidad, 
\begin_inset Formula $n>p$
\end_inset

.
 
\end_layout

\begin_layout Standard
La matriz es simétrica, tiene elementos redundantes y su dimensión es menor
 a la esperada.
\end_layout

\begin_layout Standard
Los elementos para los cuales existe la densidad son los del triángulo superior
 (el otro depende de estos, no es que no tenga densidad).
\end_layout

\begin_layout Standard
En el caso de 
\begin_inset Formula $p=1$
\end_inset

 llegamos a un múltiplo de 
\begin_inset Formula $\chi^{2}$
\end_inset

.
\end_layout

\begin_layout Subsection
Probamos que la proyección no caracteriza a las Wishart
\end_layout

\begin_layout Subsubsection
El producto entre 
\begin_inset Formula $\chi$
\end_inset

 y 
\begin_inset Formula $\beta$
\end_inset

 es una 
\begin_inset Formula $\chi$
\end_inset

.
\end_layout

\begin_layout Standard
Demostración en Mitra.
\end_layout

\begin_layout Standard
\begin_inset Formula $X\sim\chi_{n}^{2}$
\end_inset

 e 
\begin_inset Formula $Y\sim\beta\left(\frac{\nu}{2};\frac{n-\nu}{2}\right)$
\end_inset

 independientes.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
XY\sim\chi_{\nu}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left(1-Y\right)X\sim\chi_{n-\nu}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
Ambas son independientes.
\end_layout

\begin_layout Standard
Si 
\begin_inset Formula $W\sim\mathcal{W}\left(\Sigma;p;n\right)$
\end_inset

 entonces 
\begin_inset Formula $\boldsymbol{l}'W\boldsymbol{l}\sim\chi_{n}^{2}\left(\boldsymbol{l}'\Sigma\boldsymbol{l}\right)$
\end_inset


\end_layout

\begin_layout Subsubsection
La proyección de una Wishart no caracteriza una distribución.
 no alcanza con 
\begin_inset Formula $L'WL$
\end_inset

 sea 
\begin_inset Formula $\chi^{2}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $W=\mathcal{W}\left(I;p;n\right)$
\end_inset

 y 
\begin_inset Formula $Z\sim\beta\left(\frac{\nu}{2};\frac{n-\nu}{2}\right)$
\end_inset

 independientes
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
M=ZW
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $l\neq\boldsymbol{0}$
\end_inset


\end_layout

\begin_layout Standard
por la propiedad
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{l}'M\boldsymbol{l}=z\underbrace{\boldsymbol{l}'W\boldsymbol{l}}_{\chi_{n}^{2}\lVert l\rVert^{2}}\sim\chi_{\nu}^{2}\left(\lVert l\rVert^{2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Si 
\begin_inset Formula $M$
\end_inset

 fuera Wishart para una matriz de covarianzas por definir , 
\begin_inset Formula $M\sim\mathcal{W}\left(I;p;\nu\right)$
\end_inset

, podemos estudiar sus correlaciones.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{m_{ij}}{\sqrt{m_{ii}m_{jj}}}=\frac{zw_{ij}}{\sqrt{zw_{ii}zw_{jj}}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{m_{ij}}{\sqrt{m_{ii}m_{jj}}}=\frac{w_{ij}}{\sqrt{w_{ii}w_{jj}}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $W=\sum\left(x_{i}-\overline{x}\right)\left(x_{i}-\overline{x}\right)'$
\end_inset


\end_layout

\begin_layout Standard
Como vimos por la definición prededente, la densidad de la distribución
 de la 
\begin_inset Formula $\rho$
\end_inset

 como variable aleatoria depende de 
\begin_inset Formula $n$
\end_inset

.
\end_layout

\begin_layout Standard
En el miembro derecho de la expresión recién obtenida, las dimensiones son
 
\begin_inset Formula $\nu-1$
\end_inset

 (definidas como cualquier valor que hayamos elegido para la 
\begin_inset Formula $\beta$
\end_inset

) y en el izquierdo 
\begin_inset Formula $n-1$
\end_inset

.
 Entonces no fue correcto asumir que la matriz era wishart.
 
\end_layout

\begin_layout Itemize
Este contraejemplo prueba que la proyección de una Wishart no caracteriza
 una distribución.
\end_layout

\begin_layout Subsection
Teorema de Okamoto
\end_layout

\begin_layout Standard
Para probar la probabilidad nula del determinante 0 se usa inducción, en
 
\begin_inset Formula $p=1$
\end_inset

 es 0 porque es un punto de la densidad (conjunto de medida nula).
 Después hay que expresar los determinantes en forma inductiva.
\end_layout

\begin_layout Subsection
Propiedades
\end_layout

\begin_layout Subsubsection
Ejercicio de probar el producto por vector, (propiedad d).
\end_layout

\begin_layout Standard
En la siguiente propiedad donde dice que es no singular con proba 1 sólo
 usa que las densidad individuales existen.
 Esto se aclara para mostrar que es muy general.
\end_layout

\begin_layout Subsubsection
\begin_inset Formula $CWC'$
\end_inset


\end_layout

\begin_layout Standard
PAra probar el a de la página 16 uso la transformación análoga para las
 normales.
 Después se plantea 
\begin_inset Formula $W=Y'Y=CXX'C'$
\end_inset

 y cumple las hipótesis porque se conserva la media 0 y la distribución
 normal.
\end_layout

\begin_layout Standard
Ojo que la 
\begin_inset Formula $c$
\end_inset

 dice tanto que las diagonales son 
\begin_inset Formula $\chi$
\end_inset

 como que lsa extradiagonales no lo son, no se puede expresar así.
\end_layout

\begin_layout Standard
La pista para pasar de vector real a aleatorio es tomar probabilidad condicional
 (supongo que expresar en función de la condicinoada).
\end_layout

\begin_layout Subsubsection
propiedad 
\begin_inset Formula $e$
\end_inset

, sub matrices wishart independientes
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\boldsymbol{X}=\left(\begin{matrix}\boldsymbol{X}^{\left(1\right)}\\
\boldsymbol{X}^{\left(2\right)}
\end{matrix}\right)\sim\mathcal{N}\left(\left(\begin{matrix}\boldsymbol{\mu}^{\left(1\right)}\\
\boldsymbol{\mu}^{\left(2\right)}
\end{matrix}\right);\left(\begin{matrix}\Sigma_{11} & \Sigma_{12}\\
\Sigma_{21} & \Sigma_{22}
\end{matrix}\right)\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
W=\sum x_{i}x_{i}'=\left(\begin{matrix}W_{11} & W_{12}\\
W_{21} & W_{22}
\end{matrix}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
W_{11}=\sum_{i=1}^{n}X_{i}^{\left(1\right)}X_{i}^{\left(1\right)}'
\]

\end_inset


\end_layout

\begin_layout Standard
Si 
\begin_inset Formula $\Sigma_{12}=0$
\end_inset

 son independientes, dado que las normales de las cuales son función lo
 son.
\end_layout

\begin_layout Section
Descomposición de Bartlett, 23.
\end_layout

\begin_layout Itemize
Notar que habla de los 
\begin_inset Formula $b_{ii}^{2}$
\end_inset

 y de los 
\begin_inset Formula $b_{ij}$
\end_inset

.
 Los primeros están tal y como aparecen en la Wishart mientras que de los
 segundos en la wishart sólo podemos afirmar que son producto de normales
 distintas.
 Esto no importa porque 
\begin_inset Formula $W=f\left(B\right)$
\end_inset

 y en esta están bien determinados.
\end_layout

\begin_layout Standard
Demostración para el caso donde 
\begin_inset Formula $p=2$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
D\sim W\left(I_{2};p=2;n\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
D=\left(\begin{matrix}d_{11} & d_{12}\\
d_{21} & d_{22}
\end{matrix}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
La hipótesis dice que 
\begin_inset Formula $D=B'B$
\end_inset

 y que 
\begin_inset Formula $B$
\end_inset

 es triangular inferior, 
\begin_inset Formula $B=\left(\begin{matrix}b_{11} & 0\\
b_{21} & b_{22}
\end{matrix}\right)$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $D=BB'=\left(\begin{matrix}b_{11} & 0\\
b_{21} & b_{22}
\end{matrix}\right)\left(\begin{matrix}b_{11} & b_{21}\\
0 & b_{22}
\end{matrix}\right)=\left(\begin{matrix}b_{11}^{2} & b_{11}b_{21}\\
b_{11}b_{21} & b_{21}^{2}+b_{22}^{2}
\end{matrix}\right)$
\end_inset


\end_layout

\begin_layout Standard
Por las propiedades
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
d_{11}=b_{11}^{2}\sim\chi_{n}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
d_{22}=b_{21}^{2}+b_{22}^{2}\sim\chi_{n}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
Falta obtener la distribución de los no diagonales
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
d_{12}=b_{11}b_{21}
\]

\end_inset


\end_layout

\begin_layout Standard
Ya habíamos probado que cuando tenemos matriz identidad y medias cero, 
\begin_inset Formula $\Sigma_{12}=0$
\end_inset

 y entonces los diagonales son independientes.
\end_layout

\begin_layout Standard
Acá probamos que son todos independientes.
 Todos los diagonales al cuadrado son 
\begin_inset Formula $\chi^{2}$
\end_inset

.
\end_layout

\begin_layout Standard
Notar que los 
\begin_inset Formula $b_{21}^{2}+b_{22}^{2}$
\end_inset

 tienen dimensiones complementarias.
\end_layout

\begin_layout Itemize
*****
\begin_inset Formula $b_{ij}^{2}=d_{12}=b_{11}b_{21}$
\end_inset

, entonces 
\begin_inset Formula $b_{ij}\sim\mathcal{N}\left(0;1\right)$
\end_inset

.
\end_layout

\begin_layout Itemize
¿Podemos asegurar dada la definición que todas las componentes de 
\begin_inset Formula $B$
\end_inset

 tienen distribución normal?
\end_layout

\begin_layout Standard
Como todos son independientes las densidades factorizan y podemos obtener
 la densidad combinando densidades de 
\begin_inset Formula $\chi$
\end_inset

 y normales.
\end_layout

\begin_layout Subsection
Razón de los determinantes, d.24.
\end_layout

\begin_layout Standard
Aplicamos la propiedad de 
\begin_inset Quotes fld
\end_inset

estandarización
\begin_inset Quotes frd
\end_inset

 de wisharts
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
W\sim\mathcal{W}\left(\Sigma;p;n\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Sigma=CC'$
\end_inset

, siempre existe por el teorema de okamoto.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
D=C^{-1}W\left(C^{-1}\right)'\sim\mathcal{W}\left(I;p;n\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|D\right|=\left|C^{-1}W\left(C^{-1}\right)'\right|
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|D\right|=\left|C^{-1}C^{-1}'\right|\left|W\right|
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|D\right|=\frac{\left|W\right|}{\left|CC'\right|}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|D\right|=\frac{\left|W\right|}{\left|\Sigma\right|}
\]

\end_inset


\end_layout

\begin_layout Itemize
Un truco típico es reducir al caso de matriz identidad.
\end_layout

\begin_layout Standard
Como 
\begin_inset Formula $D=\mathcal{W}\left(I;p;n\right)=BB'$
\end_inset

 con 
\begin_inset Formula $B$
\end_inset

 triangular inferior.
\end_layout

\begin_layout Standard
Como es trinagular inferior conocemos la forma del determinante.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|D\right|=\left|BB'\right|=\left|B\right|^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|D\right|=\prod_{i=1}^{p}b_{ii}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
Pero estos son 
\begin_inset Formula $\chi^{2}$
\end_inset

 por Bartlett.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|D\right|=\prod_{i=1}^{p}v_{i}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
v_{i}\sim\chi_{n-i+1}^{2}
\]

\end_inset


\end_layout

\end_body
\end_document
